﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Rook
{
	public enum Coordinates
	{
		Screen,
		World
	}

	public class SuperBatch
	{
		private Camera camera;
		private SpriteBatch sb;
		private GraphicsDevice graphicsDevice;

		public SuperBatch(Camera camera, SpriteBatch sb, GraphicsDevice graphicsDevice)
		{
			this.camera = camera;
			this.sb = sb;
			this.graphicsDevice = graphicsDevice;
		}

		public void Begin(Coordinates coordinates)
		{
			Matrix transform = coordinates == Coordinates.Screen ? Matrix.Identity : camera.Transform;

			sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, transform);
		}

		public void End()
		{
			sb.End();
		}

		public void Draw(Texture2D texture, Rectangle destinationRect, Color color)
		{
			sb.Draw(texture, destinationRect, color);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect, SpriteEffects effects)
		{
			sb.Draw(texture, position, sourceRect, Color.White, 0, Vector2.Zero, 1, effects, 0);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect, Color color)
		{
			sb.Draw(texture, position, sourceRect, color, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect, Color color, float rotation)
		{
			sb.Draw(texture, position, sourceRect, color, rotation, Vector2.Zero, 1, SpriteEffects.None, 0);
		}

		public void Draw(Texture2D texture, Vector2 position, Vector2 origin, Rectangle? sourceRect, Color color, float rotation,
			float scale, SpriteEffects effects)
		{
			sb.Draw(texture, position, sourceRect, color, rotation, origin, scale, effects, 0);
		}

		public void DrawString(SpriteFont font, string value, Vector2 position, Vector2 origin, Color color, float rotation, float scale)
		{
			sb.DrawString(font, value, position, color, rotation, origin, scale, SpriteEffects.None, 0);
		}
	}
}
