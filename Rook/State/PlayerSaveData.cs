﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.State
{
	public class PlayerSaveData
	{
		public int Health { get; set; }
		public int MaxHealth { get; set; }

		// This assumes that the player can only save while on the ground.
		public int FragmentID { get; set; }
		public int BodyID { get; set; }
		public int EdgeID { get; set; }

		public float EdgeProgress { get; set; }
	}
}
