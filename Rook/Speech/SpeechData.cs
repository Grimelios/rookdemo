﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Speech
{
	public class SpeechData
	{
		private float revealRate;

		public float RevealRate
		{
			get => revealRate;
			set => revealRate = 1000 / value;
		}

		public int LineSpacing { get; set; }
	}
}
