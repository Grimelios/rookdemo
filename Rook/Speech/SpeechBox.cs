﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Json;

namespace Rook.Speech
{
	public class SpeechBox : Entity
	{
		private static SpeechData data = JsonUtilities.Deserialize<SpeechData>("Speech.json");

		private SpriteFont font;
		private List<SpeechCharacter> characterList;
		private Vector2 linePosition;

		private string[] lines;
		private string currentLine;

		private int lineIndex;
		private int characterIndex;

		public SpeechBox(int width, int height)
		{
			font = ContentLoader.LoadFont("Default");
			characterList = new List<SpeechCharacter>();
			Bounds = new Bounds(width, height);
		}

		public void Refresh(string value)
		{
			lines = Functions.WrapLines(font, value, Bounds.Width);
			currentLine = lines[0];
			lineIndex = 0;
			linePosition = Position;
			characterIndex = 0;

			Timers.Add(new Timer(data.RevealRate, RevealNextCharacter));
		}

		private bool RevealNextCharacter(float time)
		{
			float currentLength = font.MeasureString(currentLine.Substring(0, characterIndex)).X;
			char character = currentLine[characterIndex];

			characterList.Add(new SpeechCharacter(font, character)
			{
				Position = linePosition + new Vector2(currentLength, 0)
			});

			do
			{
				characterIndex++;
			}
			while (characterIndex < currentLine.Length && currentLine[characterIndex] == ' ');

			if (characterIndex == currentLine.Length)
			{
				lineIndex++;

				if (lineIndex == lines.Length)
				{
					return false;
				}

				characterIndex = 0;
				currentLine = lines[lineIndex];
				linePosition.Y += data.LineSpacing;
			}

			return true;
		}

		public override void Update(float dt)
		{
			base.Update(dt);

			characterList.ForEach(c => c.Update(dt));
		}

		public override void Draw(SuperBatch sb)
		{
			characterList.ForEach(c => c.Draw(sb));
		}
	}
}
