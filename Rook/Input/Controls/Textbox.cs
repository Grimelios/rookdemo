﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Rook.Input.Data;

namespace Rook.Input.Controls
{
	public class Textbox
	{
		private static char[] numericSpecials =
		{
			')',
			'!',
			'@',
			'#',
			'$',
			'%',
			'^',
			'&',
			'*',
			'('
		};

		private StringBuilder builder;

		// Cursor position is always before the character.
		private int cursorPosition;

		private bool insert;

		public Textbox()
		{
			builder = new StringBuilder();
		}

		public string Value
		{
			get => builder.ToString();
			set
			{
				builder.Clear();
				builder.Append(value);
			}
		}

		public int Cursor => cursorPosition;

		public bool Insert => insert;

		public void HandleKeyboard(KeyboardData data)
		{
			bool shift = data.Query(InputStates.Held, Keys.LeftShift, Keys.RightShift);
			bool capsLock = InputUtilities.CheckLock(LockKeys.CapsLock);
			bool numLock = InputUtilities.CheckLock(LockKeys.NumLock);

			// Even if numlock is off, special numpad functions can be activated by holding shift.
			if (!numLock)
			{
				numLock = shift;
			}

			bool home = data.Query(Keys.Home, InputStates.PressedThisFrame);
			bool end = data.Query(Keys.End, InputStates.PressedThisFrame);
			bool left = data.Query(Keys.Left, InputStates.PressedThisFrame);
			bool right = data.Query(Keys.Right, InputStates.PressedThisFrame);
			bool backspace = data.Query(Keys.Back, InputStates.PressedThisFrame);
			bool delete = data.Query(Keys.Delete, InputStates.PressedThisFrame);

			if (!numLock)
			{
				home |= data.Query(Keys.NumPad7, InputStates.PressedThisFrame);
				end |= data.Query(Keys.NumPad1, InputStates.PressedThisFrame);
				left |= data.Query(Keys.NumPad4, InputStates.PressedThisFrame);
				right |= data.Query(Keys.NumPad6, InputStates.PressedThisFrame);
				delete |= data.Query(Keys.Decimal, InputStates.PressedThisFrame);
			}

			// If home and end are pressed on the same frame, home takes priority.
			if (home)
			{
				cursorPosition = 0;
			}
			else if (end)
			{
				cursorPosition = builder.Length;
			}

			if (left ^ right)
			{
				if (left)
				{
					cursorPosition = cursorPosition > 0 ? --cursorPosition : 0;
				}
				else
				{
					cursorPosition = cursorPosition < builder.Length ? ++cursorPosition : builder.Length;
				}
			}

			// Backspace and delete are intentionally done after moving the cursor and before adding new characters.
			if (backspace && builder.Length > 0 && cursorPosition > 0)
			{
				builder.Remove(cursorPosition - 1, 1);
				cursorPosition--;

				return;
			}

			if (delete && builder.Length > 0 && cursorPosition < builder.Length)
			{
				builder.Remove(cursorPosition, 1);
			}

			if (data.Query(Keys.Insert, InputStates.PressedThisFrame))
			{
				insert = !insert;
			}

			foreach (Keys key in data.KeysPressedThisFrame)
			{
				char? character = GetCharacter(key, shift, capsLock, numLock);

				if (character.HasValue)
				{
					if (insert && cursorPosition < builder.Length)
					{
						builder[cursorPosition] = character.Value;
					}
					else
					{
						builder.Insert(cursorPosition, character.Value);
					}

					cursorPosition++;
				}
			}
		}

		private char? GetCharacter(Keys key, bool shift, bool capsLock, bool numLock)
		{
			string value = key.ToString();

			if (key >= Keys.A && key <= Keys.Z)
			{
				if (capsLock)
				{
					shift = !shift;
				}

				return shift ? value[0] : value.ToLower()[0];
			}

			if (key >= Keys.D0 && key <= Keys.D9)
			{
				return !shift ? value[1] : numericSpecials[value[1] - '0'];
			}

			// Special numpad functions are handled before reaching this point.
			if (numLock)
			{
				if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
				{
					return value[6];
				}

				if (key == Keys.Decimal)
				{
					return '.';
				}
			}

			switch (key)
			{
				case Keys.OemComma: return shift ? '<' : ',';
				case Keys.OemPeriod: return shift ? '>' : '.';
				case Keys.OemQuestion: return shift ? '?' : '/';
				case Keys.OemSemicolon: return shift ? ':' : ';';
				case Keys.OemQuotes: return shift ? '"' : '\'';
				case Keys.OemOpenBrackets: return shift ? '{' : '[';
				case Keys.OemCloseBrackets: return shift ? '}' : ']';
				case Keys.OemPipe: return shift ? '|' : '\\';
				case Keys.OemMinus: return shift ? '_' : '-';
				case Keys.OemPlus: return shift ? '+' : '=';
				case Keys.OemTilde: return shift ? '~' : '`';

				// These four keys work even if numlock is turned off.
				case Keys.Add: return '+';
				case Keys.Subtract: return '-';
				case Keys.Multiply: return '*';
				case Keys.Divide: return '/';

				case Keys.Space: return ' ';
			}

			return null;
		}
	}
}
