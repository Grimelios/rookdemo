﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Input.Data
{
	public enum Xbox360Buttons
	{
		A,
		B,
		X,
		Y,
		LBumper,
		RBumper,
		Start,
		Back,
		L3,
		R3,
		DPadLeft,
		DPadRight,
		DPadUp,
		DPadDown,
		BigButton
	}

	public class Xbox360Data : InputData
	{
		public Xbox360Data(InputStates a, InputStates b, InputStates x, InputStates y,
			InputStates lBumper, InputStates rBumper,
			InputStates start, InputStates back,
			InputStates l3, InputStates r3,
			InputStates dPadLeft, InputStates dPadRight, InputStates dPadUp, InputStates dPadDown,
			InputStates bigButton,
			Vector2 lStick, Vector2 rStick,
			float lTrigger, float rTrigger)
		{
			A = a;
			B = b;
			X = x;
			Y = y;
			LBumper = lBumper;
			RBumper = rBumper;
			Start = start;
			Back = back;
			L3 = l3;
			R3 = r3;
			DPadLeft = dPadLeft;
			DPadRight = dPadRight;
			DPadUp = dPadUp;
			DPadDown = dPadDown;
			BigButton = bigButton;
			LStick = lStick;
			RStick = rStick;
			LTrigger = lTrigger;
			RTrigger = rTrigger;
		}

		public InputStates A { get; }
		public InputStates B { get; }
		public InputStates X { get; }
		public InputStates Y { get; }
		public InputStates LBumper { get; }
		public InputStates RBumper { get; }
		public InputStates Start { get; }
		public InputStates Back { get; }
		public InputStates L3 { get; }
		public InputStates R3 { get; }
		public InputStates DPadLeft { get; }
		public InputStates DPadRight { get; }
		public InputStates DPadUp { get; }
		public InputStates DPadDown { get; }
		public InputStates BigButton { get; }

		public Vector2 LStick { get; }
		public Vector2 RStick { get; }

		public float LTrigger { get; }
		public float RTrigger { get; }

		public override bool Query(object data, InputStates state)
		{
			switch ((Xbox360Buttons)data)
			{
				case Xbox360Buttons.A: return A == state;
				case Xbox360Buttons.B: return B == state;
				case Xbox360Buttons.X: return X == state;
				case Xbox360Buttons.Y: return Y == state;
				case Xbox360Buttons.LBumper: return LBumper == state;
				case Xbox360Buttons.RBumper: return RBumper == state;
				case Xbox360Buttons.Start: return Start == state;
				case Xbox360Buttons.Back: return Back == state;
				case Xbox360Buttons.L3: return L3 == state;
				case Xbox360Buttons.R3: return R3 == state;
				case Xbox360Buttons.DPadLeft: return DPadLeft == state;
				case Xbox360Buttons.DPadRight: return DPadRight == state;
				case Xbox360Buttons.DPadUp: return DPadUp == state;
				case Xbox360Buttons.DPadDown: return DPadDown == state;
				case Xbox360Buttons.BigButton: return BigButton == state;
			}

			return false;
		}
	}
}
