﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Input.Data
{
	public class AggregateData
	{
		private static readonly int InputCount = Functions.EnumCount<InputTypes>();

		private InputData[] dataArray;

		public AggregateData()
		{
			dataArray = new InputData[InputCount];
		}

		public InputData this[InputTypes type]
		{
			get => dataArray[(int)type];
			set => dataArray[(int)type] = value;
		}

		public bool Query(List<InputBind> binds, InputStates state)
		{
			foreach (InputBind bind in binds)
			{
				InputData inputData = dataArray[(int)bind.InputType];

				// Null input data indicates that the input device is not currently connected.
				if (inputData != null && inputData.Query(bind.Data, state))
				{
					return true;
				}
			}

			return false;
		}

		public bool Query(List<InputBind> binds, InputStates state, out InputBind bindUsed)
		{
			foreach (InputBind bind in binds)
			{
				InputData inputData = dataArray[(int)bind.InputType];

				// Null input data indicates that the input device is not currently connected.
				if (inputData != null && inputData.Query(bind.Data, state))
				{
					bindUsed = bind;

					return true;
				}
			}

			bindUsed = null;

			return false;
		}
	}
}
