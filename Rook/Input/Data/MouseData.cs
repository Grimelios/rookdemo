﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Input.Data
{
	public enum MouseButtons
	{
		Left,
		Right,
		Middle,
		Side1,
		Side2
	}

	public class MouseData : InputData
	{
		public MouseData(InputStates leftClick, InputStates rightClick, InputStates middleClick, InputStates side1Click, InputStates side2Click,
			Vector2 screenPosition, Vector2 previousScreenPosition, Vector2 worldPosition, Vector2 previousWorldPosition)
		{
			LeftClick = leftClick;
			RightClick = rightClick;
			MiddleClick = middleClick;
			Side1Click = side1Click;
			Side2Click = side2Click;
			ScreenPosition = screenPosition;
			PreviousScreenPosition = previousScreenPosition;
			WorldPosition = worldPosition;
			PreviousWorldPosition = previousWorldPosition;
		}

		public InputStates LeftClick { get; }
		public InputStates RightClick { get; }
		public InputStates MiddleClick { get; }
		public InputStates Side1Click { get; }
		public InputStates Side2Click { get; }

		public Vector2 ScreenPosition { get; }
		public Vector2 PreviousScreenPosition { get; }
		public Vector2 WorldPosition { get; }
		public Vector2 PreviousWorldPosition { get; }

		public override bool Query(object data, InputStates state)
		{
			switch ((MouseButtons)data)
			{
				case MouseButtons.Left: return (LeftClick & state) == state;
				case MouseButtons.Right: return (RightClick & state) == state;
				case MouseButtons.Middle: return (MiddleClick & state) == state;
				case MouseButtons.Side1: return (Side1Click & state) == state;
				case MouseButtons.Side2: return (Side2Click & state) == state;
			}

			return false;
		}
	}
}
