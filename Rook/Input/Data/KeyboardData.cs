﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace Rook.Input.Data
{
	public class KeyboardData : InputData
	{
		private Dictionary<Keys, InputStates> keyStates;

		public KeyboardData(Dictionary<Keys, InputStates> keyStates, List<Keys> keysDown, List<Keys> keysPressedThisFrame,
			List<Keys> keysReleasedThisFrame)
		{
			this.keyStates = keyStates;

			KeysDown = keysDown;
			KeysPressedThisFrame = keysPressedThisFrame;
			KeysReleasedThisFrame = keysReleasedThisFrame;
		}

		public List<Keys> KeysDown { get; }
		public List<Keys> KeysPressedThisFrame { get; }
		public List<Keys> KeysReleasedThisFrame { get; }

		public bool Query(Keys key, InputStates state)
		{
			return (keyStates[key] & state) == state;
		}

		public override bool Query(object data, InputStates state)
		{
			return Query((Keys)data, state);
		}

		public bool Query(InputStates state, params Keys[] keys)
		{
			return keys.Any(k => Query(k, state));
		}
	}
}
