﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Rook.Control;
using Rook.Input.Data;
using Rook.Interfaces;
using Rook.UI.Menus;

namespace Rook.Input
{
	public class InputGenerator : IDynamic
	{
		private KeyboardState oldKS;
		private KeyboardState newKS;
		private MouseState oldMS;
		private MouseState newMS;
		private GamePadState oldPadState;
		private GamePadState newPadState;
		private MenuManager menuManager;
		private Camera camera;

		// It makes more sense to update key states each frame than to recreate the entire array every frame.
		private Dictionary<Keys, InputStates> keyStates;

		private List<Keys> keysPressedLastFrame;
		private List<Keys> keysReleasedLastFrame;

		// Storing previous world position here is easier than tracking the previous camera transform.
		private Vector2 previousWorldPosition;

		private bool paused;

		public InputGenerator(Camera camera, MenuManager menuManager)
		{
			this.camera = camera;
			this.menuManager = menuManager;
			
			keyStates = new Dictionary<Keys, InputStates>();
			
			HashSet<Keys> mappedKeys = new HashSet<Keys>();
			mappedKeys.UnionWith(Mappings.Player.GetMappedKeys());
			mappedKeys.UnionWith(Mappings.Menu.GetMappedKeys());

			// These keys are needed for textboxes.
			mappedKeys.UnionWith(new []
			{
				Keys.LeftShift,
				Keys.RightShift,
				Keys.LeftControl,
				Keys.RightControl,
				Keys.Home,
				Keys.End,
				Keys.Insert,
				Keys.Delete,
				Keys.Left,
				Keys.Right,
				Keys.NumPad0,
				Keys.NumPad1,
				Keys.NumPad4,
				Keys.NumPad6,
				Keys.NumPad7,
				Keys.Decimal
			});

			foreach (Keys key in mappedKeys)
			{
				keyStates.Add(key, InputStates.Released);
			}

			Messaging.Subscribe(MessageTypes.Pause, (data, dt) => paused = (bool)data);
		}

		public void Update(float dt)
		{
			KeyboardData keyboardData = GenerateKeyboard();
			MouseData mouseDate = GenerateMouse();
			Xbox360Data xbox360Data = GenerateXbox360();

			AggregateData aggregateData = new AggregateData
			{
				[InputTypes.Keyboard] = keyboardData,
				[InputTypes.Mouse] = mouseDate
			};

			if (xbox360Data != null)
			{
				aggregateData[InputTypes.Xbox360] = xbox360Data;
			}

			// If the game is paused, only menus can update.
			if (paused)
			{
				menuManager.HandleInput(aggregateData);

				return;
			}

			Messaging.Send(MessageTypes.Keyboard, keyboardData, dt);
			Messaging.Send(MessageTypes.Mouse, mouseDate, dt);

			if (xbox360Data != null)
			{
				Messaging.Send(MessageTypes.Xbox360, mouseDate, dt);
			}

			Messaging.Send(MessageTypes.Input, aggregateData, dt);
		}

		private KeyboardData GenerateKeyboard()
		{
			oldKS = newKS;
			newKS = Keyboard.GetState();

			Keys[] oldKeys = oldKS.GetPressedKeys();
			Keys[] newKeys = newKS.GetPressedKeys();

			List<Keys> keysPressedThisFrame = newKeys.Except(oldKeys).ToList();
			List<Keys> keysReleasedThisFrame = oldKeys.Except(newKeys).ToList();

			keysPressedLastFrame?.ForEach(k => keyStates[k] = InputStates.Held);
			keysReleasedLastFrame?.ForEach(k => keyStates[k] = InputStates.Released);
			keysPressedThisFrame.ForEach(k => keyStates[k] = InputStates.PressedThisFrame);
			keysReleasedThisFrame.ForEach(k => keyStates[k] = InputStates.ReleasedThisFrame);
			keysPressedLastFrame = keysPressedThisFrame;
			keysReleasedLastFrame = keysReleasedThisFrame;

			return new KeyboardData(keyStates, newKeys.ToList(), keysPressedThisFrame, keysReleasedThisFrame);
		}

		private MouseData GenerateMouse()
		{
			oldMS = newMS;
			newMS = Mouse.GetState();

			Vector2 screenPosition = new Vector2(newMS.X, newMS.Y);
			Vector2 previousScreenPosition = new Vector2(oldMS.X, oldMS.Y);
			Vector2 worldPosition = Vector2.Transform(screenPosition, Matrix.Invert(camera.Transform));

			InputStates leftClick = GetInputState(oldMS.LeftButton, newMS.LeftButton);
			InputStates rightClick = GetInputState(oldMS.RightButton, newMS.RightButton);
			InputStates middleClick = GetInputState(oldMS.MiddleButton, newMS.MiddleButton);
			InputStates side1Click = GetInputState(oldMS.XButton1, newMS.XButton1);
			InputStates side2Click = GetInputState(oldMS.XButton2, newMS.XButton2);

			MouseData data = new MouseData(leftClick, rightClick, middleClick, side1Click, side2Click, screenPosition,
				previousScreenPosition, worldPosition, previousWorldPosition);

			previousWorldPosition = worldPosition;

			return data;
		}

		private Xbox360Data GenerateXbox360()
		{
			oldPadState = newPadState;
			newPadState = GamePad.GetState(PlayerIndex.One);

			if (!newPadState.IsConnected)
			{
				return null;
			}

			GamePadButtons oldButtons = oldPadState.Buttons;
			GamePadButtons newButtons = newPadState.Buttons;
			GamePadDPad oldDPad = oldPadState.DPad;
			GamePadDPad newDPad = newPadState.DPad;

			InputStates a = GetInputState(oldButtons.A, newButtons.A);
			InputStates b = GetInputState(oldButtons.B, newButtons.B);
			InputStates x = GetInputState(oldButtons.X, newButtons.X);
			InputStates y = GetInputState(oldButtons.Y, newButtons.Y);
			InputStates lBumper = GetInputState(oldButtons.LeftShoulder, newButtons.LeftShoulder);
			InputStates rBumper = GetInputState(oldButtons.RightShoulder, newButtons.RightShoulder);
			InputStates start = GetInputState(oldButtons.Start, newButtons.Start);
			InputStates back = GetInputState(oldButtons.Back, newButtons.Back);
			InputStates l3 = GetInputState(oldButtons.LeftStick, newButtons.LeftStick);
			InputStates r3 = GetInputState(oldButtons.RightStick, newButtons.RightStick);
			InputStates dPadLeft = GetInputState(oldDPad.Left, newDPad.Left);
			InputStates dPadRight = GetInputState(oldDPad.Right, newDPad.Right);
			InputStates dPadUp = GetInputState(oldDPad.Up, newDPad.Up);
			InputStates dPadDown = GetInputState(oldDPad.Down, newDPad.Down);
			InputStates bigButton = GetInputState(oldButtons.BigButton, newButtons.BigButton);

			Vector2 lStick = newPadState.ThumbSticks.Left;
			Vector2 rStick = newPadState.ThumbSticks.Right;

			float lTrigger = newPadState.Triggers.Left;
			float rTrigger = newPadState.Triggers.Right;

			return new Xbox360Data(a, b, x, y, lBumper, rBumper, start, back, l3, r3, dPadLeft, dPadRight, dPadUp, dPadDown, bigButton,
				lStick, rStick, lTrigger, rTrigger);
		}

		private InputStates GetInputState(ButtonState oldState, ButtonState newState)
		{
			if (oldState == newState)
			{
				return newState == ButtonState.Pressed ? InputStates.Held : InputStates.Released;
			}

			return newState == ButtonState.Pressed ? InputStates.PressedThisFrame : InputStates.ReleasedThisFrame;
		}
	}
}
