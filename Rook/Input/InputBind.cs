﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using Rook.Input.Data;

namespace Rook.Input
{
	public class InputBind
	{
		[JsonConstructor]
		public InputBind(InputTypes inputType, string data)
		{
			InputType = inputType;
			Data = ParseData(inputType, data);
		}

		public InputBind(InputTypes inputType, object data)
		{
			InputType = inputType;
			Data = data;
		}

		public InputTypes InputType { get; set; }

		public object Data { get; set; }

		private object ParseData(InputTypes inputType, string data)
		{
			switch (inputType)
			{
				case InputTypes.Keyboard: return Functions.EnumParse<Keys>(data);
				case InputTypes.Mouse: return Functions.EnumParse<MouseButtons>(data);
				case InputTypes.Xbox360: return Functions.EnumParse<Xbox360Buttons>(data);
			}

			return null;
		}
	}
}
