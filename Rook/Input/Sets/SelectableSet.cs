﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Input.Data;
using Rook.Interfaces.Input;

namespace Rook.Input.Sets
{
	public class SelectableSet<T> where T : class, ISelectable
	{
		public SelectableSet() : this(new List<T>())
		{
		}

		public SelectableSet(List<T> items)
		{
			Items = items;
		}

		public List<T> Items { get; }
		public T HoveredItem { get; set; }
		public T FocusedItem { get; set; }

		public void HandleMouse(MouseData data)
		{
			Vector2 mousePosition = data.ScreenPosition;

			if (HoveredItem != null && !HoveredItem.Bounds.Contains(mousePosition))
			{
				HoveredItem.OnUnhover();
				HoveredItem = null;
			}

			foreach (T item in Items)
			{
				if (item.Enabled && item != HoveredItem && item.Bounds.Contains(mousePosition))
				{
					HoveredItem?.OnUnhover();
					HoveredItem = item;
					HoveredItem.OnHover();

					break;
				}
			}

			// Left click can't be rebound in this context.
			if (HoveredItem != null && data.LeftClick == InputStates.PressedThisFrame)
			{
				if (HoveredItem.OnSelect())
				{
					FocusedItem = HoveredItem;
				}
			}
		}
	}
}
