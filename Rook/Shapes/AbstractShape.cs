﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;
using Rook.Sensors;
using Rook.Shapes.Data;

namespace Rook.Shapes
{
	public enum ShapeTypes
	{
		Box,
		Circle,
		Sector
	}

	public abstract class AbstractShape : IPositionable, IRotatable
	{
		protected static ShapeHelper Helper { get; } = new ShapeHelper();

		public static AbstractShape CreateShape(ShapeData data)
		{
			switch (data.Type)
			{
				case ShapeTypes.Box:
					BoxData boxData = (BoxData)data;

					return new Box(boxData.Width, boxData.Height);

				case ShapeTypes.Circle:
					CircleData circleData = (CircleData)data;

					return new Circle(circleData.Radius);

				case ShapeTypes.Sector:
					SectorData arcData = (SectorData)data;

					return new Sector(arcData.Radius, arcData.Spread);
			}

			return null;
		}

		private float rotation;

		protected AbstractShape(ShapeTypes type)
		{
			Type = type;
		}

		public Vector2 Position { get; set; }

		public float Rotation
		{
			get => rotation;
			set => rotation = Functions.ConstrainAngle(value);
		}

		public ShapeTypes Type { get; }

		public abstract bool Contains(Vector2 point);

		public abstract bool Intersects(AbstractShape other);
	}
}
