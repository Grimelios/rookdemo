﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Shapes
{
	public class Sector : AbstractShape
	{
		public Sector(float radius, float spread) : base(ShapeTypes.Sector)
		{
			Radius = radius;
			Spread = spread;
		}

		public float Radius { get; }
		public float RadiusSquared => Radius * Radius;
		public float Spread { get; }

		public Vector2[] ComputePoints()
		{
			Vector2 d1 = Functions.ComputeDirection(Rotation + Spread / 2);
			Vector2 d2 = Functions.ComputeDirection(Rotation - Spread / 2);
			
			return new[]
			{
				Position + d1 * Radius,
				Position + d2 * Radius
			};
		}

		public override bool Contains(Vector2 point)
		{
			return !(Vector2.DistanceSquared(point, Position) > RadiusSquared) && WithinSpread(point);
		}

		public bool WithinSpread(Vector2 point)
		{
			float angle = Functions.ComputeAngle(Position, point);
			float delta = Math.Abs(Rotation - angle);

			if (delta > MathHelper.Pi)
			{
				delta = MathHelper.TwoPi - delta;
			}

			return delta <= Spread / 2;
		}

		public override bool Intersects(AbstractShape other)
		{
			return Helper.CheckIntersection(this, other);
		}
	}
}
