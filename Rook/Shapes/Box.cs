﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Shapes
{
	public class Box : AbstractShape
	{
		public Box(float width, float height, bool axisAligned = false) : base(ShapeTypes.Box)
		{
			Width = width;
			Height = height;
			AxisAligned = axisAligned;
		}

		public float Width { get; }
		public float Height { get; }

		public bool AxisAligned { get; }	

		public Box Expand(float amount)
		{
			return new Box(Width + amount * 2, Height + amount * 2, AxisAligned)
			{
				Position = Position,
				Rotation = Rotation
			};
		}

		public override bool Contains(Vector2 point)
		{
			if (!AxisAligned)
			{
				point = Position + Vector2.Transform(point - Position, Matrix.CreateRotationZ(-Rotation));
			}

			return Math.Abs(Position.X - point.X) <= Width / 2 && Math.Abs(Position.Y - point.Y) <= Height / 2;
		}

		public bool ContainsAny(Vector2[] points)
		{
			if (!AxisAligned)
			{
				Matrix matrix = Matrix.CreateRotationZ(-Rotation);

				for (int i = 0; i < points.Length; i++)
				{
					points[i] = Position + Vector2.Transform(points[i] - Position, matrix);
				}
			}

			return points.Any(p => Math.Abs(Position.X - p.X) <= Width / 2 && Math.Abs(Position.Y - p.Y) <= Height / 2);
		}

		public Vector2[] ComputePoints()
		{
			Vector2[] points =
			{
				new Vector2(-Width, -Height),
				new Vector2(Width, -Height),
				new Vector2(Width, Height),
				new Vector2(-Width, Height)
			};

			if (!AxisAligned)
			{
				Matrix rotationMatrix = Matrix.CreateRotationZ(Rotation);

				for (int i = 0; i < 4; i++)
				{
					points[i] = Vector2.Transform(points[i], rotationMatrix);
				}
			}

			for (int i = 0; i < 4; i++)
			{
				points[i] = Position + points[i] / 2;
			}

			return points;
		}

		public override bool Intersects(AbstractShape other)
		{
			return Helper.CheckIntersection(this, other);
		}
	}
}
