﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Shapes.Data
{
	public abstract class ShapeData
	{
		protected ShapeData(ShapeTypes type)
		{
			Type = type;
		}

		public ShapeTypes Type { get; }
	}
}
