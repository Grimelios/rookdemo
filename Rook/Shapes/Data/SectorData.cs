﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Shapes.Data
{
	public class SectorData : ShapeData
	{
		public SectorData(float radius, float spread) : base(ShapeTypes.Sector)
		{
			Radius = radius;
			Spread = spread;
		}

		public float Radius { get; }
		public float Spread { get; }
	}
}
