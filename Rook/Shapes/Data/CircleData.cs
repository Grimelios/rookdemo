﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Shapes.Data
{
	public class CircleData : ShapeData
	{
		public CircleData(float radius) : base(ShapeTypes.Circle)
		{
			Radius = radius;
		}

		public float Radius { get; }
	}
}
