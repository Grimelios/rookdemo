﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Rook.Input.Data;
using Rook.Interfaces;

namespace Rook.Shapes
{
	public class ShapeDemo : IRenderable
	{
		private const int ShapeCount = 20;

		private AbstractShape[] shapes;
		private ShapeVisualizer visualizer;

		public ShapeDemo()
		{
			shapes = new AbstractShape[ShapeCount];
			visualizer = new ShapeVisualizer();

			GenerateShapes();

			Messaging.Subscribe(MessageTypes.Keyboard, (data, dt) =>
			{
				KeyboardData kbData = (KeyboardData)data;

				if (kbData.Query(Keys.A, InputStates.PressedThisFrame))
				{
					GenerateShapes();
				}
			});
		}

		private void GenerateShapes()
		{
			Random random = new Random();

			int numTypes = Functions.EnumCount<ShapeTypes>();

			for (int i = 0; i < ShapeCount; i++)
			{
				AbstractShape shape = null;

				bool axisAligned = false;

				switch ((ShapeTypes)random.Next(numTypes))
				{
					case ShapeTypes.Box: shape = CreateBox(random, out axisAligned); break;
					case ShapeTypes.Circle: shape = CreateCircle(random); break;
					case ShapeTypes.Sector: shape = CreateSector(random); break;
				}

				int x = random.Next(Resolution.WindowWidth) - Resolution.WindowWidth / 2;
				int y = random.Next(Resolution.WindowHeight) - Resolution.WindowHeight / 2;

				float rotation = axisAligned ? 0 : (float)random.NextDouble() * MathHelper.TwoPi;

				shape.Position = new Vector2(x, y);
				shape.Rotation = rotation;
				shapes[i] = shape;
			}
		}

		private Box CreateBox(Random random, out bool axisAligned)
		{
			int width = random.Next(150) + 20;
			int height = random.Next(150) + 20;

			axisAligned = random.Next(2) == 1;

			return new Box(width, height, axisAligned);
		}

		private Circle CreateCircle(Random random)
		{
			int radius = random.Next(100) + 15;

			return new Circle(radius);
		}

		private Sector CreateSector(Random random)
		{
			int radius = random.Next(150) + 50;
			float spread = (float)random.NextDouble() * 0.6f + 0.5f;

			return new Sector(radius, spread);
		}

		public void Draw(SuperBatch sb)
		{
			bool[] overlaps = new bool[shapes.Length];

			for (int i = 0; i < shapes.Length; i++)
			{
				AbstractShape shape1 = shapes[i];

				for (int j = i + 1; j < shapes.Length; j++)
				{
					AbstractShape shape2 = shapes[j];

					if (shape1.Intersects(shape2))
					{
						overlaps[i] = true;
						overlaps[j] = true;
					}
				}
			}
			
			sb.Begin(Coordinates.World);

			for (int i = 0; i < shapes.Length; i++)
			{
				visualizer.Draw(sb, shapes[i], overlaps[i] ? Color.Cyan : Color.Gray);
			}

			sb.End();
		}
	}
}
