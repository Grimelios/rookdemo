﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Rook.Input.Data;
using Rook.Interfaces;

namespace Rook.Shapes
{
	public class ShapeTester : IRenderable
	{
		private const float AngularTick = 0.02f;

		private AbstractShape shape1;
		private AbstractShape shape2;
		private ShapeVisualizer visualizer;

		public ShapeTester()
		{
			shape1 = new Sector(200, 1f);
			shape2 = new Box(150, 100);
			visualizer = new ShapeVisualizer();

			Messaging.Subscribe(MessageTypes.Mouse, (data, dt) => HandleMouse((MouseData)data));
			Messaging.Subscribe(MessageTypes.Keyboard, (data, dt) => HandleKeyboard((KeyboardData)data));
		}

		private void HandleMouse(MouseData data)
		{
			shape2.Position = data.WorldPosition;
		}

		private void HandleKeyboard(KeyboardData data)
		{
			bool aDown = data.Query(Keys.A, InputStates.Held);
			bool dDown = data.Query(Keys.D, InputStates.Held);

			if (aDown ^ dDown)
			{
				shape2.Rotation += aDown ? -AngularTick : AngularTick;
			}
		}

		public void Draw(SuperBatch sb)
		{
			Color color = shape1.Intersects(shape2) ? Color.Cyan : Color.Gray;

			sb.Begin(Coordinates.World);
			visualizer.Draw(sb, shape1, color);
			visualizer.Draw(sb, shape2, color);
			sb.End();
		}
	}
}
