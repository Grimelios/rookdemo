﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Shapes
{
	public class Circle : AbstractShape
	{
		public Circle(float radius) : base(ShapeTypes.Circle)
		{
			Radius = radius;
		}

		public float Radius { get; }
		public float RadiusSquared => Radius * Radius;

		public override bool Contains(Vector2 point)
		{
			return Vector2.DistanceSquared(point, Position) <= Radius * Radius;
		}

		public override bool Intersects(AbstractShape other)
		{
			return Helper.CheckIntersection(this, other);
		}
	}
}
