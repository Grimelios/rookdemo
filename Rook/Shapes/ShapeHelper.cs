﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Collision.Shapes;
using Microsoft.Xna.Framework;

namespace Rook.Shapes
{
	public class ShapeHelper
	{
		public bool CheckIntersection(Sector sector, AbstractShape other)
		{
			switch (other.Type)
			{
				case ShapeTypes.Sector: return CheckIntersection(sector, (Sector)other);
				case ShapeTypes.Circle: return CheckIntersection(sector, (Circle)other);
				case ShapeTypes.Box: return CheckIntersection(sector, (Box)other);
			}

			return false;
		}

		public bool CheckIntersection(Circle circle, AbstractShape other)
		{
			switch (other.Type)
			{
				case ShapeTypes.Sector: return CheckIntersection((Sector)other, circle);
				case ShapeTypes.Circle: return CheckIntersection(circle, (Circle)other);
				case ShapeTypes.Box: return CheckIntersection(circle, (Box)other);
			}

			return false;
		}

		public bool CheckIntersection(Box box, AbstractShape other)
		{
			switch (other.Type)
			{
				case ShapeTypes.Sector: return CheckIntersection((Sector)other, box);
				case ShapeTypes.Circle: return CheckIntersection((Circle)other, box);
				case ShapeTypes.Box: return CheckIntersection(box, (Box)other);
			}

			return false;
		}

		private bool CheckIntersection(Sector sector1, Sector sector2)
		{
			Vector2 s1 = sector1.Position;
			Vector2 s2 = sector2.Position;

			float sumRadius = sector1.Radius + sector2.Radius;

			if (Vector2.DistanceSquared(s1, s2) > sumRadius * sumRadius)
			{
				return false;
			}

			Vector2[] points1 = sector1.ComputePoints();
			Vector2[] points2 = sector2.ComputePoints();

			// This check is intentionally put first because sectors will generally correspond to attacks, and opposing attacks will
			// more often overlap on this condition.
			if (points1.Any(sector2.Contains) || points2.Any(sector1.Contains))
			{
				return true;
			}

			Vector2 a1 = points1[0];
			Vector2 a2 = points1[1];
			Vector2 b1 = points2[0];
			Vector2 b2 = points2[1];

			if (LinesIntersect(s1, a1, s2, b1) || LinesIntersect(s1, a1, s2, b2) ||
			    LinesIntersect(s1, a2, s2, b1) || LinesIntersect(s1, a2, s2, b2))
			{
				return true;
			}

			// This center check is intentionally put last because, in practice, attack sectors will rarely (maybe never) overlap based on
			// centers.
			if (sector1.Contains(s2) || sector2.Contains(s1))
			{
				return true;
			}

			return sector1.WithinSpread(s2) && sector2.WithinSpread(s1);
		}

		private bool CheckIntersection(Sector sector, Circle circle)
		{
			Vector2 s = sector.Position;
			Vector2 c = circle.Position;

			if (circle.Contains(s) || sector.Contains(c))
			{
				return true;
			}

			float sumRadius = sector.Radius + circle.Radius;

			if (Vector2.DistanceSquared(s, c) > sumRadius * sumRadius)
			{
				return false;
			}

			Vector2[] points = sector.ComputePoints();

			for (int i = 0; i < 2; i++)
			{
				if (ComputeMinimumDistanceSquared(s, points[i], c, out Vector2 projection) <= circle.RadiusSquared)
				{
					return true;
				}
			}

			return sector.WithinSpread(c);
		}

		private bool CheckIntersection(Sector sector, Box box)
		{
			Vector2 s = sector.Position;

			if (box.Contains(s))
			{
				return true;
			}
			
			if (!box.Expand(sector.Radius).Contains(s))
			{
				return false;
			}

			if (box.ContainsAny(sector.ComputePoints()))
			{
				return true;
			}

			Vector2[] points = box.ComputePoints();

			if (points.Any(sector.Contains))
			{
				return true;
			}

			for (int i = 0; i < 4; i++)
			{
				Vector2 p1 = points[i];
				Vector2 p2 = i == 3 ? points[0] : points[i + 1];

				if (ComputeMinimumDistanceSquared(p1, p2, s, out Vector2 projection) <= sector.RadiusSquared &&
				    sector.WithinSpread(projection))
				{
					return true;
				}
			}

			return false;
		}

		private bool CheckIntersection(Circle circle1, Circle circle2)
		{
			float sumRadius = circle1.Radius + circle2.Radius;

			return Vector2.DistanceSquared(circle1.Position, circle2.Position) <= sumRadius * sumRadius;
		}

		private bool CheckIntersection(Circle circle, Box box)
		{
			Vector2 center = circle.Position;

			if (box.Contains(center))
			{
				return true;
			}

			if (!box.Expand(circle.Radius).Contains(center))
			{
				return false;
			}

			Vector2[] points = box.ComputePoints();

			if (points.Any(circle.Contains))
			{
				return true;
			}

			for (int i = 0; i < 4; i++)
			{
				Vector2 p1 = points[i];
				Vector2 p2 = i == 3 ? points[0] : points[i + 1];

				if (ComputeMinimumDistanceSquared(p1, p2, center, out Vector2 projection) <= circle.RadiusSquared)
				{
					return true;
				}
			}

			return false;
		}

		private bool CheckIntersection(Box box1, Box box2)
		{
			if (box1.AxisAligned && box2.AxisAligned)
			{
				float dX = Math.Abs(box1.Position.X - box2.Position.X);
				float dY = Math.Abs(box1.Position.Y - box2.Position.Y);

				return dX <= (box1.Width + box2.Width) / 2 && dY <= (box1.Height + box2.Height) / 2;
			}

			Vector2[] points1 = box1.ComputePoints();
			Vector2[] points2 = box2.ComputePoints();

			if (points1.Any(box2.Contains) || points2.Any(box1.Contains))
			{
				return true;
			}

			for (int i = 0; i < 4; i++)
			{
				Vector2 a1 = points1[i];
				Vector2 a2 = i == 3 ? points1[0] : points1[i + 1];

				for (int j = 0; j < 4; j++)
				{
					Vector2 b1 = points2[j];
					Vector2 b2 = j == 3 ? points2[0] : points2[j + 1];

					if (LinesIntersect(a1, a2, b1, b2))
					{
						return true;
					}
				}
			}

			return false;
		}

		private bool LinesIntersect(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2)
		{
			// See https://www.codeproject.com/Tips/862988/Find-the-Intersection-Point-of-Two-Line-Segments.
			Vector2 a = a2 - a1;
			Vector2 b = b2 - b1;
			Vector2 ba = b1 - a1;

			float cross1 = a.Cross(b);
			float cross2 = ba.Cross(a);

			// The two lines are co-linear or parallel.
			if (cross1 == 0)
			{
				return false;
			}

			float t = ba.Cross(b) / cross1;
			float u = ba.Cross(a) / cross1;

			return cross1 != 0 && (0 <= t && t <= 1) && (0 <= u && u <= 1);
		}

		private float ComputeMinimumDistanceSquared(Vector2 p1, Vector2 p2, Vector2 point, out Vector2 projection)
		{
			// See https://stackoverflow.com/a/1501725/7281613.
			float squared = Vector2.DistanceSquared(p1, p2);
			float t = MathHelper.Max(0, MathHelper.Min(1, Vector2.Dot(point - p1, p2 - p1) / squared));

			projection = p1 + t * (p2 - p1);

			return Vector2.DistanceSquared(point, projection);
		}
	}
}
