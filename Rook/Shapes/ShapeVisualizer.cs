﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Shapes
{
	public class ShapeVisualizer
	{
		private const int Segments = 30;

		public void Draw(SuperBatch sb, AbstractShape shape, Color color)
		{
			switch (shape.Type)
			{
				case ShapeTypes.Box: DrawBox(sb, (Box)shape, color); break;
				case ShapeTypes.Circle: DrawCircle(sb, (Circle)shape, color); break;
				case ShapeTypes.Sector: DrawSector(sb, (Sector)shape, color); break;
			}
		}

		private void DrawBox(SuperBatch sb, Box box, Color color)
		{
			Vector2[] points = box.ComputePoints();

			for (int i = 0; i < 4; i++)
			{
				Vector2 point1 = points[i];
				Vector2 point2 = points[i == 3 ? 0 : i + 1];

				Primitives.DrawLine(sb, point1, point2, color);
			}
		}
		
		private void DrawSector(SuperBatch sb, Sector sector, Color color)
		{
			float start = sector.Rotation - sector.Spread / 2;
			float increment = sector.Spread / Segments;

			Vector2 position = sector.Position;
			Vector2[] points = new Vector2[Segments + 1];

			for (int i = 0; i <= Segments; i++)
			{
				points[i] = position + Functions.ComputeDirection(start + increment * i) * sector.Radius;
			}

			for (int i = 0; i < Segments; i++)
			{
				Primitives.DrawLine(sb, points[i], points[i + 1], color);
			}

			Primitives.DrawLine(sb, position, points[0], color);
			Primitives.DrawLine(sb, position, points.Last(), color);
		}

		private void DrawCircle(SuperBatch sb, Circle circle, Color color)
		{
			Vector2[] points = new Vector2[Segments];

			float increment = MathHelper.TwoPi / Segments;

			for (int i = 0; i < Segments; i++)
			{
				points[i] = circle.Position + Functions.ComputeDirection(increment * i) * circle.Radius;
			}

			for (int i = 0; i < Segments; i++)
			{
				Vector2 p1 = points[i];
				Vector2 p2 = i == Segments - 1 ? points[0] : points[i + 1];

				Primitives.DrawLine(sb, p1, p2, color);
			}
		}
	}
}
