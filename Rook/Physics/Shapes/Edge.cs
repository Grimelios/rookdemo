﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Rook.Structures;

namespace Rook.Physics.Shapes
{
	public class Edge
	{
		public static void LinkCollection(Edge[] edges)
		{
			for (int i = 1; i < edges.Length - 1; i++)
			{
				Edge edge = edges[i];
				edge.Previous = edges[i - 1];
				edge.Next = edges[i + 1];
			}

			Edge first = edges[0];
			Edge last = edges.Last();

			first.Previous = last;
			first.Next = edges[1];
			last.Previous = edges[edges.Length - 2];
			last.Next = first;
		}
		
		private Vector2 start;
		private Vector2 end;

		public Edge(Vector2 start, Vector2 end)
		{
			Start = start;
			End = end;
		}

		public Vector2 Start
		{
			get => start;
			set
			{
				start = value;
				Length = Vector2.Distance(start, end);
			}
		}

		public Vector2 End
		{
			get => end;
			set
			{
				end = value;
				Length = Vector2.Distance(start, end);
			}
		}

		[JsonIgnore]
		public float Width => Math.Abs(end.X - start.X);

		[JsonIgnore]
		public float Height => Math.Abs(end.Y - start.Y);

		[JsonIgnore]
		public float? Slope
		{
			get
			{
				if (end.X == start.X)
				{
					return null;
				}

				return Height / Width;
			}
		}

		[JsonIgnore]
		public float Length { get; private set; }

		[JsonIgnore]
		public float Angle => Functions.ComputeAngle(start, end);

		[JsonIgnore]
		public Edge Previous { get; set; }

		[JsonIgnore]
		public Edge Next { get; set; }

		public float ComputeX(float y)
		{
			return start.X + (1 / Slope.Value) * (y - start.Y);
		}

		public float ComputeY(float x)
		{
			// This function should only be called for non-null slopes (non-vertical edges).
			return start.Y + Slope.Value * (x - start.X);
		}
	}
}
