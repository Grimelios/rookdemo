﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;

namespace Rook.Physics
{
	public static class PhysicsUtilities
	{
		private static World world;

		public static void Initialize(World w)
		{
			world = w;
		}

		public static Category Compose(params PhysicsGroups[] groups)
		{
			return (Category)groups.Aggregate(PhysicsGroups.None, (result, group) => result | group);
		}

		public static RaycastResults Raycast(Vector2 start, Vector2 end, Units units)
		{
			if (units == Units.Pixels)
			{
				start = PhysicsConvert.ToMeters(start);
				end = PhysicsConvert.ToMeters(end);
			}

			Vector2 hitPosition = Vector2.Zero;
			Fixture hitFixture = null;

			float closest = float.PositiveInfinity;

			world.RayCast((fixture, position, normal, fraction) =>
			{
				if (fraction < closest)
				{
					hitPosition = position;
					hitFixture = fixture;
					closest = fraction;

					return fraction;
				}

				return 1;
			}, start, end);

			if (hitFixture == null)
			{
				return null;
			}

			Body body = hitFixture.Body;
			Entity entity = body.UserData as Entity;

			return new RaycastResults(PhysicsConvert.ToPixels(hitPosition), entity, body, hitFixture.UserData);
		}
	}
}
