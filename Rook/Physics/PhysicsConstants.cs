﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;

namespace Rook.Physics
{
	[Flags]
	public enum PhysicsGroups
	{
		Player = Category.Cat1,
		Enemy = Category.Cat2,
		Character = Category.Cat3,
		World = Category.Cat4,
		None = Category.None,
		All = Category.All
	}

	public static class PhysicsConstants
	{
		public const int Gravity = 20;
		public const int GravityPixels = Gravity * PixelsPerMeter;
		public const int PixelsPerMeter = 32;
	}
}
