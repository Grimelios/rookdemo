﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Interfaces;

namespace Rook.Physics
{
	public class PhysicsVisualizer : IRenderable
	{
		private World world;
		private Color visualizerColor;

		public PhysicsVisualizer(World world)
		{
			this.world = world;

			visualizerColor = Color.White;
		}

		public bool Enabled { get; set; } = true;

		public void Draw(SuperBatch sb)
		{
			foreach (Body body in world.BodyList)
			{
				Vector2 position = PhysicsConvert.ToPixels(body.Position).ToIntegers();
				Matrix rotationMatrix = Matrix.CreateRotationZ(body.Rotation);

				foreach (Fixture fixture in body.FixtureList)
				{
					Shape shape = fixture.Shape;

					switch (shape.ShapeType)
					{
						case ShapeType.Circle:
							DrawCircle(sb, (CircleShape)shape, position);
							break;

						case ShapeType.Edge:
							DrawEdge(sb, (EdgeShape)shape, position, rotationMatrix);
							break;

						case ShapeType.Polygon:
							DrawPolygon(sb, (PolygonShape)shape, position, rotationMatrix);
							break;
					}
				}
			}
		}

		private void DrawCircle(SuperBatch sb, CircleShape shape, Vector2 position)
		{
			const int SegmentLength = 20;

			// Circle circumference = 2pi * r.
			int segments = (int)(2 * MathHelper.Pi * PhysicsConvert.ToPixels(shape.Radius) / SegmentLength);
			int pointCount = segments + 1;

			float pixelRadius = PhysicsConvert.ToPixels(shape.Radius);
			float angleIncrement = MathHelper.TwoPi / pointCount;

			Vector2[] points = new Vector2[pointCount];

			for (int i = 0; i < pointCount; ++i)
			{
				points[i] = position + Functions.ComputeDirection(angleIncrement * i) * pixelRadius;
			}

			for (int i = 0; i < pointCount; ++i)
			{
				Vector2 point1 = points[i];
				Vector2 point2 = i == pointCount - 1 ? points[0] : points[i + 1];

				Primitives.DrawLine(sb, point1, point2, visualizerColor);
			}
		}

		private void DrawEdge(SuperBatch sb, EdgeShape shape, Vector2 position, Matrix rotationMatrix)
		{
			Vector2 start = position + PhysicsConvert.ToPixels(Vector2.Transform(shape.Vertex1, rotationMatrix)).ToIntegers();
			Vector2 end = position + PhysicsConvert.ToPixels(Vector2.Transform(shape.Vertex2, rotationMatrix)).ToIntegers();

			Primitives.DrawLine(sb, start, end, visualizerColor);
		}

		private void DrawPolygon(SuperBatch sb, PolygonShape shape, Vector2 position, Matrix rotationMatrix)
		{
			Vertices vertices = shape.Vertices;
			Vector2[] points = new Vector2[vertices.Count];

			for (int i = 0; i < vertices.Count; i++)
			{
				points[i] = position + PhysicsConvert.ToPixels(Vector2.Transform(vertices[i], rotationMatrix)).ToIntegers();
			}

			for (int i = 0; i < points.Length; i++)
			{
				Vector2 point1 = points[i];
				Vector2 point2 = i == points.Length - 1 ? points[0] : points[i + 1];

				Primitives.DrawLine(sb, point1, point2, visualizerColor);
			}
		}
	}
}
