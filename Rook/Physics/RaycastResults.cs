﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;

namespace Rook.Physics
{
	public class RaycastResults
	{
		public RaycastResults(Vector2 position, Entity entity, Body body, object fixtureData)
		{
			Position = position;
			Entity = entity;
			Body = body;
			FixtureData = fixtureData;
		}

		public Vector2 Position { get; }
		public Entity Entity { get; }
		public Body Body { get; }

		public object FixtureData { get; }
	}
}
