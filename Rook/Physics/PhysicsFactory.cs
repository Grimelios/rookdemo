﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;
using Rook.Physics.Shapes;

namespace Rook.Physics
{
	public static class PhysicsFactory
	{
		private static World world;

		public static void Initialize(World w)
		{
			world = w;
		}

		public static Body CreateBody(Entity entity)
		{
			Body body = BodyFactory.CreateBody(world);
			body.UserData = entity;

			return body;
		}

		public static Body CreateDiamond(float width, float height, Units units, Entity entity)
		{
			if (units == Units.Pixels)
			{
				width = PhysicsConvert.ToMeters(width);
				height = PhysicsConvert.ToMeters(height);
			}

			Vector2 halfWidth = new Vector2(width / 2, 0);
			Vector2 halfHeight = new Vector2(0, height / 2);
			Vector2[] points =
			{
				-halfWidth,
				-halfHeight,
				halfWidth,
				halfHeight
			};

			Body body = BodyFactory.CreatePolygon(world, new Vertices(points), 1);
			body.BodyType = BodyType.Dynamic;
			body.UserData = entity;

			return body;
		}

		public static Body CreateRectangle(float width, float height, Units units, Entity entity, BodyType bodyType = BodyType.Dynamic)
		{
			return CreateRectangle(width, height, Vector2.Zero, units, entity, bodyType);
		}

		public static Body CreateRectangle(float width, float height, Vector2 position, Units units, Entity entity,
			BodyType bodyType = BodyType.Dynamic)
		{
			if (units == Units.Pixels)
			{
				width = PhysicsConvert.ToMeters(width);
				height = PhysicsConvert.ToMeters(height);
				position = PhysicsConvert.ToMeters(position);
			}

			Body body = BodyFactory.CreateRectangle(world, width, height, 1, position);
			body.BodyType = bodyType;
			body.UserData = entity;

			return body;
		}

		public static void AttachEdge(Body body, Edge edge, Units units)
		{
			Vector2 start = edge.Start;
			Vector2 end = edge.End;

			if (units == Units.Pixels)
			{
				start = PhysicsConvert.ToMeters(start);
				end = PhysicsConvert.ToMeters(end);
			}

			FixtureFactory.AttachEdge(start, end, body, edge);
		}
	}
}
