﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Interfaces;

namespace Rook
{
	public static class Functions
	{
		public static int EnumCount<T>()
		{
			return Enum.GetValues(typeof(T)).Length;
		}

		public static T EnumParse<T>(string value)
		{
			return (T)Enum.Parse(typeof(T), value);
		}

		public static float ComputeAngle(Vector2 vector)
		{
			return ComputeAngle(Vector2.Zero, vector);
		}

		public static float ComputeAngle(Vector2 start, Vector2 end)
		{
			float dX = end.X - start.X;
			float dY = end.Y - start.Y;

			return (float)Math.Atan2(dY, dX);
		}

		public static float ConstrainAngle(float angle)
		{
			if (angle < -MathHelper.Pi)
			{
				angle += MathHelper.TwoPi;
			}
			else if (angle > MathHelper.Pi)
			{
				angle -= MathHelper.TwoPi;
			}

			return angle;
		}

		public static Vector2 ComputeDirection(float angle)
		{
			float x = (float)Math.Cos(angle);
			float y = (float)Math.Sin(angle);

			return new Vector2(x, y);
		}

		public static Vector2 ComputeOrigin(int width, int height, Alignments alignment)
		{
			bool left = (alignment & Alignments.Left) == Alignments.Left;
			bool right = (alignment & Alignments.Right) == Alignments.Right;
			bool top = (alignment & Alignments.Top) == Alignments.Top;
			bool bottom = (alignment & Alignments.Bottom) == Alignments.Bottom;

			int x = left ? 0 : (right ? width : width / 2);
			int y = top ? 0 : (bottom ? height : height / 2);

			return new Vector2(x, y);
		}

		public static string RemoveExtension(string value)
		{
			return value.Substring(0, value.LastIndexOf('.'));
		}

		public static string[] WrapLines(SpriteFont font, string value, int width)
		{
			List<string> lines = new List<string>();
			StringBuilder builder = new StringBuilder();

			string[] words = value.Split(' ');

			float spaceWidth = font.MeasureString(" ").X;
			float currentWidth = 0;
			
			for (int i = 0; i < words.Length; i++)
			{
				string word = words[i];
				float wordWidth = font.MeasureString(word).X;

				if (currentWidth + wordWidth > width)
				{
					lines.Add(builder.ToString());
					currentWidth = 0;
					builder.Clear();
				}

				builder.Append(word);
				currentWidth += wordWidth + spaceWidth;

				if (i < words.Length - 1)
				{
					builder.Append(" ");
				}
			}

			if (builder.Length > 0)
			{
				lines.Add(builder.ToString());
			}

			return lines.ToArray();
		}

		public static void PositionItems<T>(List<T> items, Vector2 basePosition, Vector2 spacing) where T : class, IPositionable
		{
			for (int i = 0; i < items.Count; i++)
			{
				items[i].Position = basePosition + spacing * i;
			}
		}
	}
}
