﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;
using Rook.Interfaces;

namespace Rook
{
	public class Camera : IPositionable, IRotatable, IDynamic
	{
		public Camera()
		{
			Origin = Resolution.Center;
			Transform = Matrix.Identity;
		}

		public Vector2 Position { get; set; }
		public Vector2 Origin { get; set; }
		public Matrix Transform { get; private set; }

		public float Rotation { get; set; }
		public float Zoom { get; set; }

		public Entity Target { get; set; }

		public void Update(float dt)
		{
			if (Target != null)
			{
				Position = Target.Position;
			}
			
			Transform =
				Matrix.CreateRotationZ(Rotation) *
				Matrix.CreateTranslation(new Vector3(Origin - Position.ToIntegers(), 0));
		}
	}
}
