﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Rook.Control;
using Rook.Input;
using Rook.Interfaces;
using Rook.Json;
using Rook.Loops;
using Rook.Shapes;
using Rook.UI.Menus;

namespace Rook
{
	[Flags]
	public enum Alignments
	{
		Center = 0,
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8
	}

	public class MainGame : Game
	{
		private GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;
		private SuperBatch superBatch;
		private Camera camera;
		private GameLoop gameLoop;
		private MenuManager menuManager;
		private InputGenerator inputGenerator;

		public MainGame()
		{
			graphics = new GraphicsDeviceManager(this)
			{
				PreferredBackBufferWidth = Resolution.WindowWidth,
				PreferredBackBufferHeight = Resolution.WindowHeight
			};

			Content.RootDirectory = "Content";
			Window.Position = new Point(100);
			IsMouseVisible = true;
		}
		
		protected override void Initialize()
		{
			ContentLoader.Initialize(Content);

			camera = new Camera();
			menuManager = new MenuManager(camera);
			inputGenerator = new InputGenerator(camera, menuManager);
			//gameLoop = new TitleLoop();
			gameLoop = new GameplayLoop();
			gameLoop.Initialize(camera, menuManager);

			Messaging.Subscribe(MessageTypes.Exit, (data, dt) => Exit());
			Messaging.ProcessChanges();

			base.Initialize();
		}
		
		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			superBatch = new SuperBatch(camera, spriteBatch, GraphicsDevice);
		}
		
		protected override void UnloadContent()
		{
		}

		protected override void Update(GameTime gameTime)
		{
			float dt = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

			inputGenerator.Update(dt);
			gameLoop.Update(dt);
			camera.Update(dt);

			Messaging.ProcessChanges();
		}
		
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);
			gameLoop.Draw(superBatch);
		}
	}
}
