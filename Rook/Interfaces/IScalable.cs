﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Interfaces
{
	public interface IScalable
	{
		float Scale { get; set; }
	}
}
