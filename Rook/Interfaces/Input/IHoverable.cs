﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Interfaces.Input
{
	public interface IHoverable : IBoundable
	{
		bool Enabled { get; set; }

		void OnHover();
		void OnUnhover();
	}
}
