﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Entities;

namespace Rook.Interfaces
{
	public interface IInteractive
	{
		void OnInteract(Player player);
	}
}
