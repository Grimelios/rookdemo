﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;

namespace Rook.Interfaces
{
	public interface ITargetable
	{
		void RegisterHit(int damage, int knockback, Vector2 direction, Entity source);
	}
}
