﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Entities
{
	public static class DamageColors
	{
		static DamageColors()
		{
			Damage = Color.HotPink;
			Heal = Color.LimeGreen;
		}

		public static Color Damage { get; }
		public static Color Heal { get; }
	}
}
