﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;
using Rook.Physics;
using Rook.Physics.Shapes;

namespace Rook.Entities
{
	public class Terrain : Entity
	{
		private Edge[] edges;

		public Terrain(Edge[] edges) : base(EntityTypes.World)
		{
			this.edges = edges;

			Edge.LinkCollection(edges);

			Body = PhysicsFactory.CreateBody(this);

			foreach (Edge edge in edges)
			{
				PhysicsFactory.AttachEdge(Body, edge, Units.Pixels);
			}
		}

		public override void Update(float dt)
		{
		}

		public override void Draw(SuperBatch sb)
		{
			foreach (Edge edge in edges)
			{
				Primitives.DrawLine(sb, edge.Start, edge.End, Color.White);
			}
		}
	}
}
