﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Entities.Core;
using Rook.Interfaces;

namespace Rook.Entities.Characters
{
	public abstract class Character : LivingEntity, IInteractive
	{
		protected Character() : base(EntityTypes.Character)
		{
		}

		public void OnInteract(Player player)
		{
		}
	}
}
