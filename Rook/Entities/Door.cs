﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Interfaces;

namespace Rook.Entities
{
	public class Door : Entity, IInteractive
	{
		public Door() : base(EntityTypes.Mechanism)
		{
			Texture2D texture = ContentLoader.LoadTexture("Door");
			
			Components.Add(new Sprite(texture));
		}

		[JsonProperty]
		public Vector2 LinkedLocation { get; set; }

		public void OnInteract(Player player)
		{
			player.Position = LinkedLocation;
		}
	}
}
