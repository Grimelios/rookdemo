﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Physics;

namespace Rook.Entities
{
	public class PlayerData
	{
		private float jumpSpeed;
		private float jumpSpeedLimited;
		private float jumpDeceleration;
		private float doubleJumpSpeed;
		private float doubleJumpSpeedLimited;

		public int Health { get; set; }
		public int Acceleration { get; set; }
		public int Deceleration { get; set; }
		public int MaxSpeed { get; set; }

		public float JumpSpeed
		{
			get => jumpSpeed;
			set => jumpSpeed = PhysicsConvert.ToMeters(value);
		}

		public float JumpSpeedLimited
		{
			get => jumpSpeedLimited;
			set => jumpSpeedLimited = PhysicsConvert.ToMeters(value);
		}

		public float JumpDeceleration
		{
			get => jumpDeceleration;
			set => jumpDeceleration = PhysicsConvert.ToMeters(value);
		}

		public float DoubleJumpSpeed
		{
			get => doubleJumpSpeed;
			set => doubleJumpSpeed = PhysicsConvert.ToMeters(value);
		}

		public float DoubleJumpSpeedLimited
		{
			get => doubleJumpSpeedLimited;
			set => doubleJumpSpeedLimited = PhysicsConvert.ToMeters(value);
		}

		public float MaxSlope { get; set; }
	}
}
