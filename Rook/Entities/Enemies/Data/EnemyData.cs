﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Attacks;
using Rook.Structures;

namespace Rook.Entities.Enemies.Data
{
	using ComboMap = Dictionary<string, SinglyLinkedListNode<AttackData>>;

	public class EnemyData
	{
		public string Name { get; set; }
		public int Health { get; set; }
		public int HealthBarOffset { get; set; }
		public bool Rideable { get; set; }

		// Using this property simplifies Json data files.
		public Dictionary<string, AttackData[]> Attacks
		{
			set
			{
				ComboMap combos = new ComboMap();

				foreach (var pair in value)
				{
					combos.Add(pair.Key, new SinglyLinkedList<AttackData>(pair.Value).Head);
				}

				AttackProvider = new AttackProvider(combos);
			}
		}

		public AttackProvider AttackProvider { get; private set; }
	}
}
