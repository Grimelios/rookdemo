﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Entities.Enemies.Data
{
	public class HermitData : EnemyData
	{
		public int WakeHits { get; set; }
		public int WakeCooldown { get; set; }
	}
}
