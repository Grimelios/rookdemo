﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Entities.Core;
using Rook.Entities.Enemies.Data;
using Rook.Json;

namespace Rook.Entities.Enemies
{
	public enum EnemyTypes
	{
		Hermit,
		Phasecat,
		Slime
	}

	public abstract class Enemy : LivingEntity
	{
		private static EnemyData[] dataArray = JsonUtilities.Deserialize<EnemyData[]>("Enemies.json", true);

		public static void Initialize(EnemyHelper helper)
		{
			Helper = helper;
		}

		protected static EnemyHelper Helper { get; private set; }

		private EnemyData data;
		private HealthBar healthBar;

		protected Enemy(EnemyTypes enemyType) : base(EntityTypes.Enemy)
		{
			Initialize(dataArray[(int)enemyType]);
		}

		// Spawn position is only set once (when the enemy spawns).
		public virtual Vector2 SpawnPosition
		{
			set => Position = value;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				healthBar.Position = value - new Vector2(0, data.HealthBarOffset);

				base.Position = value;
			}
		}

		public bool Rideable { get; set; }

		protected virtual void Initialize(EnemyData data)
		{
			this.data = data;

			Health = data.Health;
			MaxHealth = data.Health;
			Rideable = data.Rideable;
			healthBar = new HealthBar(this);
		}

		protected abstract void AI(float dt);

		public override void Update(float dt)
		{
			AI(dt);

			if (healthBar.Visible)
			{
				healthBar.Update(dt);
			}

			base.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			base.Draw(sb);

			if (healthBar.Visible)
			{
				healthBar.Draw(sb);
			}
		}
	}
}
