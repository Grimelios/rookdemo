﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Entities.Enemies.Data;
using Rook.Physics;
using Rook.Sensors;
using Rook.Shapes;

namespace Rook.Entities.Enemies
{
	public class Hermit : Enemy
	{
		private const int RaycastRange = 10;

		private List<Timer> wakeTimers;
		private HermitData data;

		private int wakeHits;

		public Hermit() : base(EnemyTypes.Hermit)
		{
			Components.Add(new Sprite("Enemies/Hermit"));
			Body = PhysicsFactory.CreateRectangle(2, 2, Units.Meters, this, BodyType.Static);
			Sensors.Add(SensorFactory.CreateSensor(SensorTypes.Enemy, new Box(40, 40), this));
			wakeTimers = new List<Timer>();
		}

		public override Vector2 SpawnPosition
		{
			set
			{
				RaycastResults results = PhysicsUtilities.Raycast(value, value + new Vector2(0, RaycastRange), Units.Meters);
				Position = results.Position;
			}
		}

		protected override void Initialize(EnemyData data)
		{
			this.data = (HermitData)data;

			base.Initialize(data);
		}

		protected override void AI(float dt)
		{
		}

		public override void RegisterHit(int damage, int knockback, Vector2 direction, Entity source)
		{
			wakeHits++;

			Timer timer = new Timer(data.WakeCooldown, time =>
			{
				wakeHits = 0;
				wakeTimers.RemoveAt(0);
			});

			Timers.Add(timer);
			wakeTimers.Add(timer);

			if (wakeHits == data.WakeHits)
			{
				WakeUp();
			}

			base.RegisterHit(damage, knockback, direction, source);
		}

		private void WakeUp()
		{
			wakeTimers.ForEach(t => Timers.Remove(t));
		}
	}
}
