﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Entities.Enemies
{
	public class Slime : Enemy
	{
		public Slime() : base(EnemyTypes.Slime)
		{
		}

		protected override void AI(float dt)
		{
		}
	}
}
