﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;
using Rook.Physics;
using Rook.Sensors;
using Rook.Shapes;

namespace Rook.Entities.Enemies
{
	public class Phasecat : Enemy
	{
		public Phasecat() : base(EnemyTypes.Phasecat)
		{
			Texture2D texture = ContentLoader.LoadTexture("Enemies/Phasecat");

			Body = PhysicsFactory.CreateDiamond(texture.Width, texture.Height, Units.Pixels, this);
			Body.Friction = 0;
			Body.FixedRotation = true;
			Body.CollisionCategories = (Category)PhysicsGroups.Enemy;
			Body.CollidesWith = (Category)PhysicsGroups.World;

			Bounds = new Bounds(texture.Width, texture.Height);
			Sensors.Add(SensorFactory.CreateSensor(SensorTypes.Enemy, new Box(texture.Width, texture.Height, true), this));
			Components.Add(new Sprite(texture));
		}

		protected override void AI(float dt)
		{
		}
	}
}
