﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Entities.Weapons
{
	public class SwordData
	{
		public int Damage { get; set; }
		public int Knockback { get; set; }

		public string Name { get; set; }
	}
}
