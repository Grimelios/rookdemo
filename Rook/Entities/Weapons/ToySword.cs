﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Attacks;
using Rook.Sensors;
using Rook.Shapes;

namespace Rook.Entities.Weapons
{
	public class ToySword : Sword
	{
		public ToySword() : base(SwordTypes.ToySword)
		{
		}
	}
}
