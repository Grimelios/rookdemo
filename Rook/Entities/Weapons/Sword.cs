﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Attacks;
using Rook.Entities.Core;
using Rook.Json;
using Rook.Sensors;
using Rook.Shapes.Data;

namespace Rook.Entities.Weapons
{
	public enum SwordTypes
	{
		ToySword,
		Broadsword,
		Stoneblade
	}

	public abstract class Sword : Entity
	{
		private static SwordData[] dataArray = JsonUtilities.Deserialize<SwordData[]>("Swords.json");

		private SwordData data;

		protected Sword(SwordTypes swordType)
		{
			data = dataArray[(int)swordType];
			SwordType = swordType;
		}

		public SwordTypes SwordType { get; }

		public string Name => data.Name;

		public AttackProvider Provider { get; set; }

		public ActiveAttack Attack(Vector2 direction, Player player, out string animation)
		{
			AttackData attackData = new AttackData
			{
				Animation = null,
				Delay = 0,
				Lifetime = 100
			};

			ActiveAttack attack = new ActiveAttack(attackData, new SectorData(150, 0.5f), player)
			{
				Damage = data.Damage,
				Knockback = data.Knockback,
				Direction = direction
			};

			attack.Sensor.Rotation = Functions.ComputeAngle(direction);
			animation = attackData.Animation;

			return attack;
		}
	}
}
