﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Interfaces;

namespace Rook.Entities.Interactive
{
	public class SaveStatue : Entity, IInteractive
	{
		public SaveStatue() : base(EntityTypes.Mechanism)
		{
			Components.Add(new Sprite("SaveStatue"));
		}

		public void OnInteract(Player player)
		{
			Messaging.Send(MessageTypes.Save, null);
		}
	}
}
