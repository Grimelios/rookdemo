﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Json;

namespace Rook.Entities.Interactive
{
	public class Lever : Entity
	{
		private static LeverData data = JsonUtilities.Deserialize<LeverData>("Lever.json");

		private Sprite baseSprite;
		private Sprite leverSprite;

		public Lever() : base(EntityTypes.Mechanism)
		{
			Texture2D texture = ContentLoader.LoadTexture("Lever");
			Atlas atlas = new Atlas("LeverAtlas.json");

			baseSprite = new Sprite(texture, atlas["Base"], Alignments.Left)
			{
				Rotation = -MathHelper.PiOver2
			};

			leverSprite = new Sprite(texture, atlas["Lever"], Alignments.Bottom)
			{
				Rotation = -data.RestingAngle
			};

			Components.Add(baseSprite);
			Components.Add(leverSprite);

			UseCustomComponentPositioning = true;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				leverSprite.Position = value;
				baseSprite.Position = value;

				base.Position = value;
			}
		}
	}
}
