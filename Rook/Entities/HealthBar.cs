﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Json;

namespace Rook.Entities
{
	public class HealthBar : Entity
	{
		private static HealthBarData data = JsonUtilities.Deserialize<HealthBarData>("HealthBar.json");

		private LivingEntity parent;
		private Bounds innerBounds;

		public HealthBar(LivingEntity parent) : base(EntityTypes.Information)
		{
			this.parent = parent;

			Bounds = new Bounds(data.Width, data.Height);
			innerBounds = new Bounds(data.Width, data.Height);

			parent.OnHealthChange += OnHealthChange;
			parent.OnMaxHealthChange += OnMaxHealthChange;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				base.Position = value;

				innerBounds.Location = Bounds.Location;
			}
		}

		public bool Visible { get; set; } = true;

		private void OnHealthChange(int health, int previous)
		{
			int fill = (int)((float)health / parent.MaxHealth * Bounds.Width);

			innerBounds.Width = fill;
		}

		private void OnMaxHealthChange(int maxHealth, int previous)
		{
		}

		public override void Draw(SuperBatch sb)
		{
			Primitives.FillBounds(sb, innerBounds, Color.Red);
		}
	}
}
