﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Entities.Core
{
	using EntityMap = Dictionary<EntityTypes, Entity[]>;

	public class SceneFragment
	{
		public EntityMap[] Layers { get; set; }
	}
}
