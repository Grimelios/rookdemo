﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Entities.Core
{
	public class SceneLayer : IDynamic, IRenderable
	{
		private List<Entity>[] entities;
		private List<Entity> addList;
		private List<Entity> removeList;

		private EntityTypes[] updateOrder;
		private EntityTypes[] drawOrder;

		public SceneLayer(EntityTypes[] updateOrder, EntityTypes[] drawOrder)
		{
			this.updateOrder = updateOrder;
			this.drawOrder = drawOrder;

			// Subtracting one accounts for EntityTypes.None.
			entities = new List<Entity>[Functions.EnumCount<EntityTypes>() - 1];
			addList = new List<Entity>();
			removeList = new List<Entity>();

			foreach (EntityTypes type in updateOrder.Union(drawOrder))
			{
				entities[(int)type] = new List<Entity>();
			}
		}

		public void Add(Entity entity)
		{
			addList.Add(entity);
		}

		public void Add(IEnumerable<Entity> entities)
		{
			addList.AddRange(entities);
		}

		public void Remove(Entity entity)
		{
			removeList.Add(entity);
		}

		public void Update(float dt)
		{
			foreach (EntityTypes type in updateOrder)
			{
				entities[(int)type].ForEach(e => e.Update(dt));
			}

			removeList.ForEach(e => entities[(int)e.EntityType].Remove(e));
			removeList.Clear();

			addList.ForEach(e => entities[(int)e.EntityType].Add(e));
			addList.Clear();
		}

		public void Draw(SuperBatch sb)
		{
			foreach (EntityTypes type in drawOrder)
			{
				entities[(int)type].ForEach(e => e.Draw(sb));
			}
		}
	}
}
