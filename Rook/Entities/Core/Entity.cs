﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Rook.Core;
using Rook.Interfaces;
using Rook.Physics;
using Rook.Sensors;

namespace Rook.Entities.Core
{
	public enum EntityTypes
	{
		Player,
		Character,
		Enemy,
		Boss,
		Mechanism,
		Information,
		World,
		None
	}

	public abstract class Entity : IRotatable, IBoundable, IDynamic, IRenderable, IDisposable
	{
		private Body body;
		private Vector2 position;
		private List<Component2D> components;
		private List<Timer> timers;
		private List<Sensor> sensors;

		private float rotation;

		private bool selfUpdate;

		protected Entity(EntityTypes entityType = EntityTypes.None)
		{
			EntityType = entityType;
		}

		protected List<Component2D> Components => components ?? (components = new List<Component2D>());
		protected List<Timer> Timers => timers ?? (timers = new List<Timer>());
		protected List<Sensor> Sensors => sensors ?? (sensors = new List<Sensor>());

		protected bool UseCustomComponentPositioning { get; set; }
		protected bool UseCustomSensorPositioning { get; set; }

		public virtual Vector2 Position
		{
			get => position;
			set
			{
				position = value;
				Bounds.Center = value.ToPoint();

				if (!UseCustomComponentPositioning)
				{
					components?.ForEach(c => c.Position = value);
				}

				if (!UseCustomSensorPositioning)
				{
					sensors?.ForEach(s => s.Position = value);
				}

				if (body != null && !selfUpdate)
				{
					body.Position = PhysicsConvert.ToMeters(value);
				}
			}
		}

		public Vector2 Velocity
		{
			get => body != null ? PhysicsConvert.ToPixels(body.LinearVelocity) : Vector2.Zero;
			set => body.LinearVelocity = PhysicsConvert.ToMeters(value);
		}
		
		public float Rotation
		{
			get => rotation;
			set
			{
				value = Functions.ConstrainAngle(value);
				rotation = value;

				if (!UseCustomComponentPositioning)
				{
					components?.ForEach(c => c.Rotation = value);
				}

				if (!UseCustomSensorPositioning)
				{
					sensors?.ForEach(s => s.Rotation = value);
				}

				if (body != null && !selfUpdate)
				{
					body.Rotation = value;
				}
			}
		}

		public Bounds Bounds { get; protected set; } = new Bounds();

		public EntityTypes EntityType { get; }

		public Body Body
		{
			get => body;
			set
			{
				body = value;
				body.OnCollision += (fixture1, fixture2, contact) =>
				{
					contact.GetWorldManifold(out Vector2 normal, out FixedArray2<Vector2> points);

					return OnCollision(fixture2.Body.UserData as Entity, fixture2.Body, fixture2.UserData,
						PhysicsConvert.ToPixels(points[0]), normal);
				};
			}
		}

		public Scene Scene { get; set; }

		public bool OnGround { get; protected set; }

		protected virtual bool OnCollision(Entity entity, Body body, object fixtureData, Vector2 position, Vector2 normal)
		{
			return true;
		}

		public virtual void Dispose()
		{
			body?.Dispose();
			sensors.ForEach(s => s.Dispose());
		}

		public virtual void Update(float dt)
		{
			if (timers != null)
			{
				timers.ForEach(t => t.Update(dt));

				for (int i = timers.Count - 1; i >= 0; i--)
				{
					if (timers[i].Complete)
					{
						timers.RemoveAt(i);
					}
				}
			}

			if (body != null && !OnGround)
			{
				selfUpdate = true;
				Position = PhysicsConvert.ToPixels(body.Position);
				Rotation = body.Rotation;
				selfUpdate = false;
			}
		}

		public virtual void Draw(SuperBatch sb)
		{
			components.ForEach(c => c.Draw(sb));
		}
	}
}
