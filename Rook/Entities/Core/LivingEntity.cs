﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Interfaces;

namespace Rook.Entities.Core
{
	public abstract class LivingEntity : Entity, ITargetable
	{
		public delegate void HealthChangeHandler(int current, int previous);
		public delegate void DeathHandler();

		private int health;
		private int maxHealth;

		protected LivingEntity(EntityTypes entityType = EntityTypes.None) : base(entityType)
		{
		}

		public int Health
		{
			get => health;
			set
			{
				int previous = health;

				health = value;

				if (health != previous)
				{
					OnHealthChange?.Invoke(health, previous);
				}
			}
		}

		public int MaxHealth
		{
			get => maxHealth;
			set
			{
				int previous = maxHealth;

				maxHealth = value;

				if (maxHealth != previous)
				{
					OnMaxHealthChange?.Invoke(maxHealth, previous);
				}
			}
		}

		public event HealthChangeHandler OnHealthChange;
		public event HealthChangeHandler OnMaxHealthChange;
		public event DeathHandler OnDeath;

		public virtual void RegisterHit(int damage, int knockback, Vector2 direction, Entity source)
		{
			Health = MathHelper.Max(Health - damage, 0);

			if (Health == 0)
			{
				OnDeath?.Invoke();
			}

			Scene.Add(new DamageValue(damage, Position, direction * 100, 0.1f));
		}

		public void Kill()
		{
			OnDeath?.Invoke();
		}
	}
}
