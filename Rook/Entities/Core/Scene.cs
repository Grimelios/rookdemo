﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;
using Rook.Json;

namespace Rook.Entities.Core
{
	using EntityMap = Dictionary<EntityTypes, Entity[]>;

	public class Scene : IDynamic, IRenderable
	{
		private SceneLayer[] layers;

		public Scene()
		{
			layers = JsonUtilities.Deserialize<SceneLayer[]>("Layers.json");
		}

		public void Add(Entity entity, int layer = 0)
		{
			layers[layer].Add(entity);
			entity.Scene = this;
		}

		public void Remove(Entity entity, int layer = 0)
		{
			layers[layer].Remove(entity);
		}

		public void Load(string fragment)
		{
			SceneFragment frag = JsonUtilities.Deserialize<SceneFragment>("Fragments/" + fragment, true);
			EntityMap[] fragmentLayers = frag.Layers;

			for (int i = 0; i < fragmentLayers.Length; i++)
			{
				SceneLayer layer = layers[i];
				EntityMap map = fragmentLayers[i];

				foreach (KeyValuePair<EntityTypes, Entity[]> pair in map)
				{
					layer.Add(pair.Value);
				}
			}
		}

		public void Update(float dt)
		{
			foreach (SceneLayer layer in layers)
			{
				layer.Update(dt);
			}
		}

		public void Draw(SuperBatch sb)
		{
			foreach (SceneLayer layer in layers)
			{
				layer.Draw(sb);
			}
		}
	}
}
