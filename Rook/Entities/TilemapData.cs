﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Rook.Physics.Shapes;

namespace Rook.Entities
{
	public class TilemapData
	{
		public TilemapData(int width, int height, int tileSize, int[,] tiles, SpriteEffects[,] flipData, string tilesheet, Edge[][] edges)
		{
			Width = width;
			Height = height;
			TileSize = tileSize;
			Tiles = tiles;
			FlipData = flipData;
			Tilesheet = tilesheet;
			Edges = edges;
		}

		public int Width { get; }
		public int Height { get; }
		public int TileSize { get; }
		public int[,] Tiles { get; }

		public SpriteEffects[,] FlipData { get; }

		public string Tilesheet { get; }

		public Edge[][] Edges { get; }
	}
}
