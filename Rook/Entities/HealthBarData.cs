﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Entities
{
	public class HealthBarData
	{
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
