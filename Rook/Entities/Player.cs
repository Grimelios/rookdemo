﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Attacks;
using Rook.Control;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Entities.Weapons;
using Rook.Input;
using Rook.Input.Data;
using Rook.Interfaces;
using Rook.Json;
using Rook.Physics;
using Rook.Physics.Shapes;
using Rook.Sensors;
using Rook.Shapes;

namespace Rook.Entities
{
	public enum PlayerSkills
	{
		Run,
		Jump,
		DoubleJump
	}

	public class Player : LivingEntity
	{
		private const int RunIndex = (int)PlayerSkills.Run;
		private const int JumpIndex = (int)PlayerSkills.Jump;
		private const int DoubleJumpIndex = (int)PlayerSkills.DoubleJump;
		private const int MaximumJumps = 2;

		private PlayerData playerData;
		private PlayerControls controls;
		private RunController runController;
		private Sensor playerSensor;
		private ActiveAttack activeAttack;
		private Body terrainBody;

		private bool[] skillsUnlocked;
		private bool[] skillsEnabled;
		private bool jumpActive;
		private bool jumpDecelerating;

		private int jumpsRemaining;

		private InputBind jumpBindUsed;

		private Sword[] swords;
		private Sword equippedSword;

		public Player() : base(EntityTypes.Player)
		{
			controls = Mappings.Player;
			playerData = JsonUtilities.Deserialize<PlayerData>("Player.json");
			skillsUnlocked = new bool[Functions.EnumCount<PlayerSkills>()];
			skillsEnabled = new bool[skillsUnlocked.Length];
			swords = new Sword[Functions.EnumCount<SwordTypes>()];

			Texture2D texture = ContentLoader.LoadTexture("Player");

			int width = texture.Width;
			int height = texture.Height;

			Body = PhysicsFactory.CreateDiamond(width, height, Units.Pixels, this);
			Body.FixedRotation = true;
			Body.Friction = 0;
			Body.ManuallyControlled = true;
			Body.MaximumSpeed = PhysicsConvert.ToMeters(playerData.MaxSpeed);
			Body.CollisionCategories = (Category)PhysicsGroups.Player;
			Body.CollidesWith = (Category)PhysicsGroups.World;

			Health = playerData.Health;
			MaxHealth = Health;
			Bounds = new Bounds(width, height);
			playerSensor = SensorFactory.CreateSensor(SensorTypes.Player, new Box(width, height), this);

			runController = new RunController(this)
			{
				Acceleration = playerData.Acceleration,
				Deceleration = playerData.Deceleration,
				MaxSpeed = playerData.MaxSpeed
			};

			Components.Add(new Sprite(texture));

			Messaging.Subscribe(MessageTypes.Input, (data, dt) => HandleInput((AggregateData)data, dt));
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				playerSensor.Position = value;

				// Attacks are assumed centered on the player.
				if (activeAttack != null)
				{
					activeAttack.Sensor.Position = value;
				}

				base.Position = value;
			}
		}

		public Vector2 GroundPoint
		{
			set => Position = value - new Vector2(0, Bounds.Height / 2);
		}

		public int Acceleration => playerData.Acceleration;
		public int Deceleration => playerData.Deceleration;
		public int MaxSpeed => playerData.MaxSpeed;

		protected override bool OnCollision(Entity entity, Body body, object fixtureData, Vector2 position, Vector2 normal)
		{
			if (entity.EntityType == EntityTypes.World)
			{
				Edge edge = (Edge)fixtureData;

				if (!edge.Slope.HasValue || Math.Abs(edge.Slope.Value) > playerData.MaxSlope)
				{
					return true;
				}

				float deltaY = Velocity.Y - entity.Velocity.Y;
				float positionDelta = position.Y - (PhysicsConvert.ToPixels(Body.Position.Y) + Bounds.Height / 2);

				if (deltaY >= 0 && positionDelta >= 0)
				{
					OnLanding(entity, body, (Edge)fixtureData);
				}

				return true;
			}

			return true;
		}

		private void OnLanding(Entity entity, Body body, Edge edge)
		{
			// If double jump is unlocked, single jump is assumed unlocked as well.
			if (skillsUnlocked[DoubleJumpIndex])
			{
				skillsEnabled[JumpIndex] = true;
				skillsEnabled[DoubleJumpIndex] = false;
				jumpsRemaining = MaximumJumps;
			}
			else if (skillsUnlocked[JumpIndex])
			{
				skillsEnabled[JumpIndex] = true;
				jumpsRemaining = 1;
			}

			OnGround = true;
			jumpActive = false;
			jumpDecelerating = false;
			jumpBindUsed = null;
			terrainBody = body;

			float bodySpeed = Body.LinearVelocity.X;

			Body.LinearVelocity = Vector2.Zero;
			Body.IgnoreGravity = true;
			Body.Accelerating = false;
			Body.IgnoreCollisionWith(terrainBody);

			runController.SignalLanding(entity, edge, Body.Position.X, bodySpeed);
		}

		private void HandleInput(AggregateData data, float dt)
		{
			if (OnGround && data.Query(controls.Interact, InputStates.PressedThisFrame))
			{
				Sensor other = playerSensor.ContactList.FirstOrDefault(s => (s.SensorType & SensorTypes.Interactive) == s.SensorType);
				((IInteractive)other?.UserData)?.OnInteract(this);
			}

			HandleAttack(data, (MouseData)data[InputTypes.Mouse]);

			if (skillsEnabled[RunIndex])
			{
				HandleRunning(data, dt);
			}

			if (jumpActive)
			{
				float limitSpeed = -(jumpsRemaining == MaximumJumps - 1 ? playerData.JumpSpeedLimited : playerData.DoubleJumpSpeedLimited);

				// If jump is released while paused, ReleasedThisFrame will never be triggered.
				if (data[jumpBindUsed.InputType].Query(jumpBindUsed.Data, InputStates.ReleasedThisFrame, InputStates.Released))
				{
					jumpActive = false;
					jumpDecelerating = Body.LinearVelocity.Y < limitSpeed;
				}
			}
			else if (jumpsRemaining > 0)
			{
				HandleJumping(data);
			}
		}

		private void HandleAttack(AggregateData data, MouseData mouseData)
		{
			// Even if aiming with the mouse, attack can still be rebound if desired.
			if (equippedSword != null && activeAttack == null && data.Query(controls.Attack, InputStates.PressedThisFrame))
			{
				// Attacks require aim. On KBM, that can be done using keys (similar to joystick aiming) or mouse.
				Vector2 direction = Vector2.Normalize(mouseData.WorldPosition - Position);

				activeAttack = equippedSword.Attack(direction, this, out string animation);
				activeAttack.Sensor.IgnoreCollisionWith(playerSensor);
				activeAttack.OnCompletion += () => activeAttack = null;
			}
		}

		private void HandleRunning(AggregateData data, float dt)
		{
			Xbox360Data controllerData = (Xbox360Data)data[InputTypes.Xbox360];

			bool left = false;
			bool right = false;

			if (controllerData != null)
			{
				Vector2 stick = controllerData.LStick;

				// This moves the player left or right if the stick is within the left or right quadrants (divided diagonally).
				if (Math.Abs(stick.X) >= Math.Abs(stick.Y) && stick.X != 0)
				{
					if (Math.Sign(stick.X) == 1)
					{
						right = true;
					}
					else
					{
						left = true;
					}
				}
			}

			left |= data.Query(controls.RunLeft, InputStates.Held);
			right |= data.Query(controls.RunRight, InputStates.Held);

			if (left ^ right)
			{
				runController.Accelerating = true;
				runController.Direction = left ? -1 : 1;
				Body.Accelerating = true;
			}
			else
			{
				runController.Accelerating = false;
				runController.Direction = 0;
				Body.Accelerating = false;
			}

			runController.Update(dt);
		}

		private void HandleJumping(AggregateData data)
		{
			if (data.Query(controls.Jump, InputStates.PressedThisFrame, out jumpBindUsed))
			{
				if (skillsEnabled[JumpIndex])
				{
					Jump();
				}
				// This point can only be reached if double jump is unlocked.
				else
				{
					DoubleJump();
				}
			}
		}

		private void Jump()
		{
			OnGround = false;
			skillsEnabled[JumpIndex] = false;
			skillsEnabled[DoubleJumpIndex] = skillsUnlocked[DoubleJumpIndex];
			jumpActive = true;
			jumpsRemaining--;

			Vector2 velocity = Body.LinearVelocity;
			velocity.Y = -playerData.JumpSpeed;

			// The player can jump for a short time after running off an edge, so the terrain body might already be null.
			if (terrainBody != null)
			{
				Body.IgnoreGravity = false;
				Body.RestoreCollisionWith(terrainBody);
				velocity.X = PhysicsConvert.ToMeters(runController.CurrentSpeed);
			}

			Body.LinearVelocity = velocity;
			runController.SignalSeparation();
		}

		private void DoubleJump()
		{
			jumpsRemaining--;
			jumpActive = true;
			jumpDecelerating = false;
			skillsEnabled[DoubleJumpIndex] = jumpsRemaining == 0;
			Body.LinearVelocity = new Vector2(Body.LinearVelocity.X, -playerData.DoubleJumpSpeed);
		}

		public void SignalDropoff()
		{
			Body.IgnoreGravity = false;
			Body.RestoreCollisionWith(terrainBody);
			Body.LinearVelocity = new Vector2(PhysicsConvert.ToMeters(runController.CurrentSpeed), 0);
			OnGround = false;
			terrainBody = null;
			jumpsRemaining--;
			skillsEnabled[JumpIndex] = false;
		}

		public void Unlock(PlayerSkills skill)
		{
			int index = (int)skill;

			skillsUnlocked[index] = true;
			skillsEnabled[index] = CheckSkillEnabledOnUnlock(skill);
		}

		public void Equip(Sword sword)
		{
			swords[(int)sword.SwordType] = sword;
			equippedSword = sword;
		}

		private bool CheckSkillEnabledOnUnlock(PlayerSkills skill)
		{
			switch (skill)
			{
				case PlayerSkills.Jump: return OnGround;
				case PlayerSkills.DoubleJump: return !OnGround;
			}

			return true;
		}

		public override void Dispose()
		{
			playerSensor.Dispose();
			Body.Dispose();
		}

		public override void Update(float dt)
		{
			activeAttack?.Update(dt);

			if (jumpDecelerating)
			{
				Body.ApplyRawForce(new Vector2(0, playerData.JumpDeceleration));

				float limitSpeed = -(jumpsRemaining == MaximumJumps - 1 ? playerData.JumpSpeedLimited : playerData.DoubleJumpSpeedLimited);

				if (Body.LinearVelocity.Y >= limitSpeed)
				{
					Body.LinearVelocity = new Vector2(Body.LinearVelocity.X, limitSpeed);
					jumpDecelerating = false;
				}
			}

			base.Update(dt);
		}
	}
}
