﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Rook.Entities.Core;
using Rook.Json;
using Rook.Physics;
using Rook.Physics.Shapes;

namespace Rook.Entities
{
	public class Tilemap : Entity
	{
		private Body[] bodies;
		private Texture2D tilesheet;

		private int width;
		private int height;
		private int tileSize;
		private int tilesPerRow;
		private int[,] tiles;

		private SpriteEffects[,] flipData;

		[JsonConstructor]
		public Tilemap(string filename) : this(JsonUtilities.Deserialize<TilemapData>("Tilemaps/" + filename))
		{
		}

		public Tilemap(TilemapData data) : base(EntityTypes.World)
		{
			width = data.Width;
			height = data.Height;
			tileSize = data.TileSize;
			tiles = data.Tiles;
			flipData = data.FlipData;
			tilesheet = ContentLoader.LoadTexture("Tilesheets/" + data.Tilesheet);
			tilesPerRow = tilesheet.Width / tileSize;
			Bounds.Width = width * tileSize;
			Bounds.Height = height * tileSize;

			if (data.Edges != null)
			{
				Edge[][] edges = data.Edges;

				bodies = new Body[edges.Length];

				for (int i = 0; i < bodies.Length; i++)
				{
					Body body = PhysicsFactory.CreateBody(this);
					Edge[] edgeArray = edges[i];

					// Smaller platforms can have a single one-way edge.
					if (edgeArray.Length > 1)
					{
						Edge.LinkCollection(edgeArray);
					}

					foreach (Edge edge in edgeArray)
					{
						PhysicsFactory.AttachEdge(body, edge, Units.Pixels);
					}

					body.CollisionCategories = (Category)PhysicsGroups.World;
					body.CollidesWith = (Category)PhysicsGroups.All;

					bodies[i] = body;
				}
			}
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				if (bodies != null)
				{
					Vector2 position = PhysicsConvert.ToMeters(value);

					foreach (Body body in bodies)
					{
						body.Position = position;
					}
				}

				base.Position = value;
			}
		}

		public override void Dispose()
		{
			if (bodies != null)
			{
				foreach (Body body in bodies)
				{
					body.Dispose();
				}
			}
		}

		public override void Draw(SuperBatch sb)
		{
			Rectangle sourceRect = new Rectangle(0, 0, tileSize, tileSize);

			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					int tile = tiles[j, i];

					if (tile >= 0)
					{
						sourceRect.X = tile % tilesPerRow * tileSize;
						sourceRect.Y = tile / tilesPerRow * tileSize;

						sb.Draw(tilesheet, Position + new Vector2(j, i) * tileSize, sourceRect, flipData[j, i]);
					}
				}
			}
		}
	}
}
