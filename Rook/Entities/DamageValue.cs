﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Physics;

namespace Rook.Entities
{
	public class DamageValue : Entity
	{
		private const int Lifetime = 800;

		private Vector2 velocity;

		private float angularVelocity;

		public DamageValue(int value, Vector2 position, Vector2 velocity, float angularVelocity) :
			base(EntityTypes.Information)
		{
			this.velocity = velocity;
			this.angularVelocity = angularVelocity;

			Components.Add(new SpriteText("Default", value.ToString())
			{
				// Negative damage values are considered healing.
				Color = value > 0 ? DamageColors.Damage : DamageColors.Heal
			});

			Timers.Add(new Timer(Lifetime, time => Scene.Remove(this)));
			Position = position;
		}

		public override void Update(float dt)
		{
			velocity.Y += PhysicsConstants.GravityPixels * dt;
			Position += velocity * dt;
			Rotation += angularVelocity * dt;

			base.Update(dt);
		}
	}
}
