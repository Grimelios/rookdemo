﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Rook.Control;
using Rook.Input;
using Rook.Input.Data;

namespace Rook.Entities
{
	public class PlayerControls : MappingClass
	{
		public List<InputBind> RunLeft { get; set; }
		public List<InputBind> RunRight { get; set; }
		public List<InputBind> Jump { get; set; }
		public List<InputBind> Interact { get; set; }
		public List<InputBind> Attack { get; set; }

		public override HashSet<Keys> GetMappedKeys()
		{
			List<InputBind> binds = new List<InputBind>();
			binds.AddRange(RunLeft);
			binds.AddRange(RunRight);
			binds.AddRange(Jump);
			binds.AddRange(Interact);
			binds.AddRange(Attack);

			return GetMappedKeys(binds);
		}
	}
}
