﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Structures;

namespace Rook.Attacks
{
	using ComboMap = Dictionary<string, SinglyLinkedListNode<AttackData>>;

	public class AttackProvider
	{
		private ComboMap combos;

		public AttackProvider(ComboMap combos)
		{
			this.combos = combos;
		}

		public AttackData TriggerAttack(string group)
		{
			return combos[group].Data;
		}
	}
}
