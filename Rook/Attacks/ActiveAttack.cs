﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Entities.Core;
using Rook.Interfaces;
using Rook.Sensors;
using Rook.Shapes;
using Rook.Shapes.Data;

namespace Rook.Attacks
{
	public class ActiveAttack : IDynamic
	{
		public delegate void AttackCompletionHandler();

		private AttackData attackData;
		private Entity source;
		private Timer timer;
		private List<LivingEntity> targetsHit;

		public ActiveAttack(AttackData attackData, ShapeData shapeData, Entity source)
		{
			this.attackData = attackData;
			this.source = source;

			Sensor = SensorFactory.CreateSensor(SensorTypes.Attack, AbstractShape.CreateShape(shapeData), source);
			Sensor.OnContact += OnContact;

			timer = new Timer(attackData.Lifetime, time =>
			{
				Sensor.Dispose();
				OnCompletion?.Invoke();
			});

			targetsHit = new List<LivingEntity>();
		}

		public Sensor Sensor { get; }

		public int Damage { get; set; }
		public int Knockback { get; set; }

		public Vector2 Direction { get; set; }

		public event AttackCompletionHandler OnCompletion;

		private void OnContact(Sensor other)
		{
			if ((other.SensorType & SensorTypes.Living) > 0)
			{
				LivingEntity entity = (LivingEntity)other.UserData;

				if (!targetsHit.Contains(entity))
				{
					entity.RegisterHit(Damage, Knockback, Direction, source);
					targetsHit.Add(entity);
				}
			}
		}

		public void Update(float dt)
		{
			timer.Update(dt);
		}
	}
}
