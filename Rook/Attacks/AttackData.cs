﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Entities.Core;
using Rook.Shapes;

namespace Rook.Attacks
{
	public class AttackData
	{
		public string Animation { get; set; }

		public int Delay { get; set; }
		public int Lifetime { get; set; }
	}
}
