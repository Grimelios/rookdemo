﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using Rook.Data;

namespace Rook
{
	using PropertyMap = Dictionary<string, string>;
	using PropertyCache = Dictionary<string, Dictionary<string, string>>;

	public static class Properties
	{
		private static PropertyCache cache = new PropertyCache();

		public static object[] Load(string filename, DataField[] fields)
		{
			PropertyMap map = Load(filename);

			object[] data = new object[fields.Length];

			for (int i = 0; i < fields.Length; i++)
			{
				DataField field = fields[i];
				data[i] = ParseData(field.Type, map[field.Key]);
			}

			return data;
		}

		private static object ParseData(FieldTypes fieldType, string value)
		{
			switch (fieldType)
			{
				case FieldTypes.Integer: return int.Parse(value);
				case FieldTypes.Float: return float.Parse(value);
				case FieldTypes.Color: return Parsing.ParseColor(value);
			}

			return null;
		}

		public static PropertyMap Load(string filename)
		{
			if (!cache.TryGetValue(filename, out PropertyMap map))
			{
				map = new PropertyMap();

				foreach (string line in File.ReadAllLines(Paths.Properties + filename))
				{
					if (line.Length == 0)
					{
						continue;
					}

					string[] tokens = line.Split('=');
					string key = tokens[0].TrimEnd();
					string value = tokens[1].TrimStart();

					map.Add(key, value);
				}

				cache.Add(filename, map);
			}

			return map;
		}
	}
}
