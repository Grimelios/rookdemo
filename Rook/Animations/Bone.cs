﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;

namespace Rook.Animations
{
	public class Bone
	{
		private List<Bone> children;

		public Vector2 LocalPosition { get; set; }
		public Vector2 AbsolutePosition { get; set; }

		public float LocalRotation { get; set; }
		public float AbsoluteRotation { get; set; }

		public List<Bone> Children => children ?? (children = new List<Bone>());

		public void ApplyAbsoluteTransforms()
		{
			Matrix rotationMatrix = Matrix.CreateRotationZ(AbsoluteRotation);

			Children.ForEach(c =>
			{
				c.AbsolutePosition = AbsolutePosition + Vector2.Transform(c.LocalPosition, rotationMatrix);
				c.AbsoluteRotation = AbsoluteRotation + c.LocalRotation;

				if (c.children != null)
				{
					c.ApplyAbsoluteTransforms();
				}
			});
		}
	}
}
