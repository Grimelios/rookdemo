﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Animations
{
	public class AnimationGroup
	{
		public AnimationGroup(int[] bones, Dictionary<string, Animation> animations)
		{
			Bones = bones;
			Animations = animations;
		}

		public int[] Bones { get; }

		public Dictionary<string, Animation> Animations { get; }
	}
}
