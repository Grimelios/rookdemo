﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Animations
{
	public class AnimationPlayer : IDynamic
	{
		private Timeline[] timelines;
		private Bone[] bones;

		private float elapsed;
		private float duration;

		private int[] currentKeyframes;

		public AnimationPlayer(Skeleton parent)
		{
			// The full skeleton reference isn't needed (since only bones are updated).
			bones = parent.Bones;
		}

		public bool Complete { get; private set; }

		public void Play(Animation animation)
		{
			timelines = animation.Timelines;
			duration = animation.Length;
			currentKeyframes = new int[timelines.Length];

			for (int i = 0; i < timelines.Length; i++)
			{
				Keyframe[] keyframes = timelines[i].Keyframes;

				// If a timeline doesn't have a keyframe at time zero, the last keyframe is lerped forward (wrapping around).
				if (keyframes[0].Time > 0)
				{
					currentKeyframes[i] = keyframes.Length - 1;
				}
			}

			Complete = false;
		}

		public void Update(float dt)
		{
			elapsed += dt * 1000;

			for (int i = 0; i < timelines.Length; i++)
			{
				Keyframe[] keyframes = timelines[i].Keyframes;

				int currentIndex = currentKeyframes[i];
				int nextIndex = currentIndex == keyframes.Length - 1 ? 0 : ++currentIndex;

				if (elapsed >= keyframes[nextIndex].Time)
				{
					currentIndex++;
					nextIndex = nextIndex == keyframes.Length - 1 ? 0 : ++nextIndex;
				}

				Keyframe key1 = keyframes[currentIndex];
				Keyframe key2 = keyframes[nextIndex];

				int time = key1.Time;
				float amount = (elapsed < time ? duration - time + elapsed : elapsed - time) / key1.Duration;

				Transform transform = Transform.Lerp(key1.Transform, key2.Transform, amount);
				Bone bone = bones[i];
				bone.LocalPosition = transform.Position;
				bone.LocalRotation = transform.Rotation;
			}
		}
	}
}
