﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Animations
{
	public class Keyframe
	{
		public Keyframe(Transform transform, int time, int duration)
		{
			Transform = transform;
			Time = time;
			Duration = duration;
		}

		public Transform Transform { get; }

		public int Time { get; }
		public int Duration { get; }
	}
}
