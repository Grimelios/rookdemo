﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;
using Rook.Json;

namespace Rook.Animations
{
	public class Skeleton : IPositionable, IRotatable, IDynamic
	{
		private Dictionary<string, Animation> animationMap;
		private AnimationPlayer animationPlayer;
		private Bone masterBone;

		public Skeleton(string group)
		{
			AnimationGroup animationGroup = JsonUtilities.Deserialize<AnimationGroup>("Animations/" + group + ".json");

			// This is an array of bone parent indices.
			int[] rawBones = animationGroup.Bones;

			Bones = new Bone[rawBones.Length];
			animationMap = animationGroup.Animations;
			animationPlayer = new AnimationPlayer(this);

			for (int i = 0; i < rawBones.Length; i++)
			{
				Bones[i] = new Bone();
			}

			for (int i = 0; i < rawBones.Length; i++)
			{
				int parent = rawBones[i];

				// This assumes that all skeletons will have a single master body (or root body).
				if (parent == -1)
				{
					masterBone = Bones[i];
				}
				else
				{
					Bones[rawBones[i]].Children.Add(Bones[i]);
				}
			}
		}

		public Vector2 Position { get; set; }

		public float Rotation { get; set; }

		public Bone[] Bones { get; }

		public void Play(string animation)
		{
			animationPlayer.Play(animationMap[animation]);
		}

		public void Update(float dt)
		{
			if (!animationPlayer.Complete)
			{
				animationPlayer.Update(dt);
			}

			masterBone.ApplyAbsoluteTransforms();
		}
	}
}
