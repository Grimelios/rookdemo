﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Animations
{
	public class Animation
	{
		public Animation(Timeline[] timelines, int length, bool looped)
		{
			Timelines = timelines;
			Length = length;
			Looped = looped;
		}

		public Timeline[] Timelines { get; }

		public int Length { get; }

		public bool Looped { get; }
	}
}
