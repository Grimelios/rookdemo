﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.UI.Menus;

namespace Rook.Loops
{
	public class SplashLoop : GameLoop
	{
		public override void Initialize(Camera camera, MenuManager menuManager)
		{
		}

		public override void Update(float dt)
		{
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.Screen);
			sb.End();
		}
	}
}
