﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Entities;
using Rook.Entities.Core;
using Rook.Entities.Enemies;
using Rook.Entities.Weapons;
using Rook.Physics;
using Rook.Physics.Shapes;
using Rook.Sensors;
using Rook.Speech;
using Rook.UI;
using Rook.UI.Hud;
using Rook.UI.Menus;
using Rook.Utility;

namespace Rook.Loops
{
	public class GameplayLoop : GameLoop
	{
		private Scene scene;
		private SensorManager sensorManager;
		private SensorVisualizer sensorVisualizer;
		private PhysicsAccumulator accumulator;
		private PhysicsVisualizer physicsVisualizer;
		private HeadsUpDisplay headsUpDisplay;
		private DebugDisplay debugDisplay;
		private MenuManager menuManager;
		private SpeechBox speechBox;

		private bool paused;

		public GameplayLoop()
		{
			Messaging.Subscribe(MessageTypes.Pause, (data, dt) => paused = (bool)data);
		}

		public override void Initialize(Camera camera, MenuManager menuManager)
		{
			this.menuManager = menuManager;

			new TiledConverter().Convert("Demo.tmx");

			World world = new World(new Vector2(0, PhysicsConstants.Gravity));
			PhysicsFactory.Initialize(world);
			PhysicsUtilities.Initialize(world);

			sensorManager = new SensorManager();
			sensorVisualizer = new SensorVisualizer(sensorManager);
			accumulator = new PhysicsAccumulator(world);
			physicsVisualizer = new PhysicsVisualizer(world);
			headsUpDisplay = new HeadsUpDisplay();
			debugDisplay = new DebugDisplay
			{
				Position = new Vector2(20)
			};

			speechBox = new SpeechBox(300, 200);
			speechBox.Refresh("Alright, I've been thinking. When life gives you lemons, don't make lemonade! Make life take the lemons back! Get mad! I don't want your damn lemons; what am I supposed to do with these? Demand to see life's manager! Make life rue the day it thought it could give Cave Johnson lemons! Do you know who I am? I'm the man who's gonna burn your house down... with the lemons! I'm gonna get my engineers to invent a combustible lemon that burns your house down!");

			SensorFactory.Initialize(sensorManager);

			scene = new Scene();
			scene.Load("Clocktower1.json");

			Player player = new Player();
			player.Unlock(PlayerSkills.Run);
			player.Unlock(PlayerSkills.Jump);
			player.Unlock(PlayerSkills.DoubleJump);
			player.Equip(new ToySword());

			headsUpDisplay.GetElement<PlayerHealthDisplay>().Player = player;

			Enemy hermit = new Hermit
			{
				SpawnPosition = Vector2.Zero
			};

			Enemy phasecat = new Phasecat
			{
				SpawnPosition = new Vector2(0, 400)
			};

			Door door = new Door
			{
				Position = new Vector2(400, 0),
				LinkedLocation = new Vector2(2000, 0)
			};

			Enemy.Initialize(new EnemyHelper(player));

			scene.Add(player);
			scene.Add(hermit);
			scene.Add(phasecat);
			scene.Add(door);

			camera.Target = player;
		}

		public override void Update(float dt)
		{
			if (paused)
			{
				menuManager.Update(dt);

				return;
			}

			accumulator.Update(dt);
			scene.Update(dt);
			sensorManager.Update(dt);
			headsUpDisplay.Update(dt);
			speechBox.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.World);
			scene.Draw(sb);
			speechBox.Draw(sb);

			if (physicsVisualizer.Enabled)
			{
				physicsVisualizer.Draw(sb);
			}

			if (sensorVisualizer.Enabled)
			{
				sensorVisualizer.Draw(sb);
			}

			sb.End();

			sb.Begin(Coordinates.Screen);
			headsUpDisplay.Draw(sb);
			debugDisplay.Draw(sb);
			debugDisplay.Clear();
			sb.End();
		}
	}
}
