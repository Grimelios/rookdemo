﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;
using Rook.UI.Menus;

namespace Rook.Loops
{
	public abstract class GameLoop : IDynamic, IRenderable
	{
		public abstract void Initialize(Camera camera, MenuManager menuManager);
		public abstract void Update(float dt);
		public abstract void Draw(SuperBatch sb);
	}
}
