﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Input.Controls;
using Rook.UI.Menus;
using Rook.UI.Screens;

namespace Rook.Loops
{
	public class TitleLoop : GameLoop
	{
		private MenuManager menuManager;
		private TextboxTester textboxTester;
		private ControllerMappingScreen controllerMappingScreen;

		public override void Initialize(Camera camera, MenuManager menuManager)
		{
			this.menuManager = menuManager;

			textboxTester = new TextboxTester();
			//controllerMappingScreen = new ControllerMappingScreen();
		}

		public override void Update(float dt)
		{
			//menuManager.Update(dt);
			//controllerMappingScreen.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.Screen);
			//menuManager.Draw(sb);
			textboxTester.Draw(sb);
			//controllerMappingScreen.Draw(sb);
			sb.End();
		}
	}
}
