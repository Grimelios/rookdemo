﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.UI.Menus
{
	public class MenuData
	{
		public MenuData()
		{
			ItemSpacing = int.Parse(Properties.Load("Menu.properties")["Item.Spacing"]);
		}

		public int ItemSpacing { get; }
	}
}
