﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.UI.Menus.Items;

namespace Rook.UI.Menus
{
	public class TitleMenu : Menu
	{
		public TitleMenu()
		{
			string[] names =
			{
				"Continue",
				"New game",
				"Load game",
				"Settings",
				"Exit"
			};

			List<MenuItem> items = new List<MenuItem>();

			for (int i = 0; i < names.Length; i++)
			{
				items.Add(new BasicMenuItem(names[i], i));
			}

			SelectedIndex = 1;
			items[SelectedIndex].OnHover();
			items[0].Enabled = false;

			Initialize(items);
		}

		public override void Submit(int index)
		{
			switch (index)
			{
				// Continue
				case 0: break;

				// New game
				case 1: break;

				// Load game
				case 2: break;

				// Settings
				case 3: break;
				
				// Exit
				case 4: Messaging.Send(MessageTypes.Exit, null); break;
			}
		}
	}
}
