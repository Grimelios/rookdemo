﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Rook.Control;
using Rook.Core;
using Rook.Input.Data;
using Rook.Input.Sets;
using Rook.UI.Menus.Items;

namespace Rook.UI.Menus
{
	public abstract class Menu : Container2D
	{
		private static MenuControls controls;
		private static MenuData data;

		static Menu()
		{
			controls = Mappings.Menu;
			data = new MenuData();
		}

		private List<MenuItem> items;
		private SelectableSet<MenuItem> selectableSet;

		protected virtual void Initialize(List<MenuItem> items)
		{
			this.items = items;

			items.ForEach(i => i.Parent = this);
			selectableSet = new SelectableSet<MenuItem>(items);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				Functions.PositionItems(items, value, new Vector2(0, data.ItemSpacing));
				Bounds = Bounds.Enclose(items);

				base.Position = value;
			}
		}

		public int SelectedIndex { get; set; }

		public MenuManager Manager { get; set; }
		public MenuItem ActiveItem { get; set; }

		public virtual void HandleInput(AggregateData data)
		{
			if (data.Query(controls.Back, InputStates.PressedThisFrame))
			{
				return;
			}

			if (data.Query(controls.Proceed, InputStates.PressedThisFrame))
			{
				Submit(SelectedIndex);

				return;
			}

			// Basic menu navigation is intentionally left unmappable so that players don't accidentally lock themselves out of menus.
			HandleKeyboard((KeyboardData)data[InputTypes.Keyboard]);
		}

		private void HandleKeyboard(KeyboardData data)
		{
			/*
			bool up = data[Keys.W] == InputStates.PressedThisFrame || data[Keys.Up] == InputStates.PressedThisFrame;
			bool down = data[Keys.S] == InputStates.PressedThisFrame || data[Keys.Down] == InputStates.PressedThisFrame;

			if (up ^ down)
			{
				if (up)
				{
					do
					{
						SelectedIndex = SelectedIndex == 0 ? items.Count - 1 : --SelectedIndex;
					}
					while (!items[SelectedIndex].Enabled);
				}
				else
				{
					do
					{
						SelectedIndex = SelectedIndex == items.Count - 1 ? 0 : ++SelectedIndex;
					}
					while (!items[SelectedIndex].Enabled);
				}
			}
			*/
		}

		public void HandleMouse(MouseData data)
		{
			selectableSet.HandleMouse(data);
		}

		public abstract void Submit(int index);

		public override void Update(float dt)
		{
			items.ForEach(i => i.Update(dt));
		}

		public override void Draw(SuperBatch sb)
		{
			items.ForEach(i => i.Draw(sb));
		}
	}
}
