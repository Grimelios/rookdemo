﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Rook.Control;
using Rook.Input;
using Rook.Input.Data;

namespace Rook.UI.Menus
{
	public class MenuControls : MappingClass
	{
		public List<InputBind> Up { get; set; }
		public List<InputBind> Down { get; set; }
		public List<InputBind> Left { get; set; }
		public List<InputBind> Right { get; set; }
		public List<InputBind> Proceed { get; set; }
		public List<InputBind> Back { get; set; }
		public List<InputBind> Pause { get; set; }

		public override HashSet<Keys> GetMappedKeys()
		{
			List<InputBind> binds = new List<InputBind>();
			binds.AddRange(Up);
			binds.AddRange(Down);
			binds.AddRange(Left);
			binds.AddRange(Right);
			binds.AddRange(Proceed);
			binds.AddRange(Back);
			binds.AddRange(Pause);

			return GetMappedKeys(binds);
		}
	}
}
