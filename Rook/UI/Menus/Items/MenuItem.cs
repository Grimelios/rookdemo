﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Core;
using Rook.Interfaces.Input;

namespace Rook.UI.Menus.Items
{
	public abstract class MenuItem : Container2D, ISelectable
	{
		protected static MenuItemData Data { get; } = new MenuItemData();

		private int index;

		private bool enabled;

		protected MenuItem(int index)
		{
			this.index = index;

			enabled = true;
		}

		public virtual bool Enabled
		{
			get => enabled;
			set => enabled = value;
		}

		public Menu Parent { get; set; }

		public virtual void OnHover()
		{
		}

		public virtual void OnUnhover()
		{
		}

		public virtual bool OnSelect()
		{
			Parent.Submit(index);

			return false;
		}
	}
}
