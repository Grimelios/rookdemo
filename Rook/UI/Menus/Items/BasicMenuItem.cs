﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;

namespace Rook.UI.Menus.Items
{
	public class BasicMenuItem : MenuItem
	{
		private SpriteText spriteText;

		public BasicMenuItem(string value, int index) : base(index)
		{
			SpriteFont font = ContentLoader.LoadFont("Default");
			spriteText = new SpriteText(font, value)
			{
				Color = Data.BaseColor
			};

			Bounds = new Bounds(font.MeasureString(value).ToPoint());
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		public override bool Enabled
		{
			get => base.Enabled;
			set
			{
				spriteText.Color = value ? Data.BaseColor : Data.DisabledColor;

				base.Enabled = value;
			}
		}

		public override void OnHover()
		{
			spriteText.Color = Data.HighlightColor;
		}

		public override void OnUnhover()
		{
			spriteText.Color = Data.BaseColor;
		}

		public override void Draw(SuperBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
