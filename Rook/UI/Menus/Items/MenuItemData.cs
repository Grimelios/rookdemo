﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Data;

namespace Rook.UI.Menus.Items
{
	public class MenuItemData
	{
		public MenuItemData()
		{
			object[] data = Properties.Load("Menu.properties", new []
			{
				new DataField(FieldTypes.Color, "Item.Base.Color"),
				new DataField(FieldTypes.Color, "Item.Highlight.Color"),
				new DataField(FieldTypes.Color, "Item.Disabled.Color")
			});

			BaseColor = (Color)data[0];
			HighlightColor = (Color)data[1];
			DisabledColor = (Color)data[2];
		}

		public Color BaseColor { get; }
		public Color HighlightColor { get; }
		public Color DisabledColor { get; }
	}
}
