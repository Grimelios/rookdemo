﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Control;
using Rook.Input.Data;
using Rook.Interfaces;

namespace Rook.UI.Menus
{
	public class MenuManager : IDynamic, IRenderable
	{
		private Camera camera;
		private Stack<Menu> menuStack;
		private MenuControls controls;
		private Menu activeMenu;

		private bool paused;

		public MenuManager(Camera camera)
		{
			this.camera = camera;

			controls = Mappings.Menu;

			/*
			TitleMenu titleMenu = new TitleMenu
			{
				Manager = this,
				Position = new Vector2(100)
			};

			controls = Mappings.Menu;
			menuStack = new Stack<Menu>();
			menuStack.Push(titleMenu);
			activeMenu = titleMenu;
			*/

			Messaging.Subscribe(MessageTypes.Input, (data, dt) => HandleInput((AggregateData)data));
		}

		public void HandleInput(AggregateData data)
		{
			if (data.Query(controls.Pause, InputStates.PressedThisFrame))
			{
				paused = !paused;

				Messaging.Send(MessageTypes.Pause, paused);
			}

			if (!paused)
			{
				return;
			}
		}

		public void Update(float dt)
		{
			/*
			foreach (Menu menu in menuStack)
			{
				menu.Update(dt);
			}
			*/
		}

		public void Draw(SuperBatch sb)
		{
			/*
			foreach (Menu menu in menuStack)
			{
				menu.Draw(sb);
			}
			*/
		}
	}
}
