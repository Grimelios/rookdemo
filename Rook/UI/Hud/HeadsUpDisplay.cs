﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;
using Rook.Json;

namespace Rook.UI.Hud
{
	public class HeadsUpDisplay : IDynamic, IRenderable
	{
		private HudElement[] elements;

		public HeadsUpDisplay()
		{
			elements = JsonUtilities.Deserialize<HudElement[]>("Hud.json", true);

			int width = Resolution.WindowWidth;
			int height = Resolution.WindowHeight;

			foreach (HudElement element in elements)
			{
				Alignments alignment = element.Alignment;

				bool left = (alignment & Alignments.Left) == Alignments.Left;
				bool right = (alignment & Alignments.Right) == Alignments.Right;
				bool top = (alignment & Alignments.Top) == Alignments.Top;
				bool bottom = (alignment & Alignments.Bottom) == Alignments.Bottom;

				int offsetX = element.OffsetX;
				int offsetY = element.OffsetY;
				int x = left ? offsetX : (right ? width - offsetX : width / 2);
				int y = top ? offsetY : (bottom ? height - offsetY : height / 2);

				element.Position = new Vector2(x, y);
				element.Initialize();
			}
		}

		public T GetElement<T>() where T : HudElement
		{
			return (T)elements.FirstOrDefault(t => t is T);
		}

		public void Update(float dt)
		{
			foreach (HudElement element in elements)
			{
				element.Update(dt);
			}
		}

		public void Draw(SuperBatch sb)
		{
			foreach (HudElement element in elements)
			{
				if (element.Visible)
				{
					element.Draw(sb);
				}
			}
		}
	}
}
