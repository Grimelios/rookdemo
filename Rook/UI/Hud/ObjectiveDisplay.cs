﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;

namespace Rook.UI.Hud
{
	public class ObjectiveDisplay : HudElement
	{
		private SpriteText objectiveText;
		private Timer visibilityTimer;

		public ObjectiveDisplay()
		{
			objectiveText = new SpriteText("Default", "Objective: reach the manor.", Alignments.Top);
			visibilityTimer = new Timer(3000, time => Visible = false);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				objectiveText.Position = value;

				base.Position = value;
			}
		}

		public override void Update(float dt)
		{
			if (visibilityTimer != null)
			{
				visibilityTimer.Update(dt);

				if (visibilityTimer.Complete)
				{
					visibilityTimer = null;
				}
			}
		}

		public override void Draw(SuperBatch sb)
		{
			objectiveText.Draw(sb);
		}
	}
}
