﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;
using Rook.Entities;

namespace Rook.UI.Hud
{
	public class PlayerHealthDisplay : HudElement
	{
		private Player player;
		private SpriteText healthText;
		private SpriteText separator;
		private SpriteText maxHealthText;

		private int characterWidth;

		public PlayerHealthDisplay()
		{
			SpriteFont font = ContentLoader.LoadFont("Debug");

			healthText = new SpriteText(font, null);
			separator = new SpriteText(font, "/");
			maxHealthText = new SpriteText(font, null);
			characterWidth = (int)font.MeasureString("A").X;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				healthText.Position = value;
				separator.Position = value + new Vector2(characterWidth * 3, 0);
				maxHealthText.Position = value + new Vector2(characterWidth * 4, 0);
			}
		}

		public Player Player
		{
			set
			{
				player = value;
				player.OnHealthChange += OnHealthChange;
				player.OnMaxHealthChange += OnMaxHealthChange;
				player.OnDeath += OnDeath;

				healthText.Value = player.Health.ToString();
				maxHealthText.Value = player.MaxHealth.ToString();
			}
		}

		private void OnHealthChange(int health, int previous)
		{
			healthText.Value = health.ToString();
		}

		private void OnMaxHealthChange(int maxHealth, int previous)
		{
			maxHealthText.Value = maxHealth.ToString();
		}

		private void OnDeath()
		{
			healthText.Value = "0";
		}

		public override void Draw(SuperBatch sb)
		{
			healthText.Draw(sb);
			separator.Draw(sb);
			maxHealthText.Draw(sb);
		}
	}
}
