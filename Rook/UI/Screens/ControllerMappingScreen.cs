﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;

namespace Rook.UI.Screens
{
	public class ControllerMappingScreen : Screen
	{
		private Sprite controllerImage;
		private ActionMapping mapping;

		public ControllerMappingScreen()
		{
			controllerImage = new Sprite("UI/Mapping/Xbox360")
			{
				Position = Resolution.WindowCenter
			};

			mapping = new ActionMapping(ActionTypes.Movement, "Jump")
			{
				Position = Resolution.WindowCenter - new Vector2(0, 250)
			};
		}

		public override void Draw(SuperBatch sb)
		{
			controllerImage.Draw(sb);
			mapping.Draw(sb);

			base.Draw(sb);
		}
	}
}
