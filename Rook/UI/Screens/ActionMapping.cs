﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;

namespace Rook.UI.Screens
{
	public enum ActionTypes
	{
		Movement,
		Combat,
		Menuing
	}

	public class ActionMapping : Container2D
	{
		private const int IconSize = 32;
		private const int FullWidth = 200;

		private Sprite actionSprite;
		private SpriteText actionText;

		public ActionMapping(ActionTypes actionType, string action)
		{
			Rectangle sourceRect = new Rectangle((int)actionType * IconSize, 0, IconSize, IconSize);
			actionSprite = new Sprite("UI/Mapping/ActionTypes", sourceRect);
			actionText = new SpriteText("Default", action, Alignments.Left);
			Bounds = new Bounds(FullWidth, IconSize + UIConstants.Padding * 2);
			Centered = true;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				actionSprite.Position = new Vector2(value.X - FullWidth / 2 + UIConstants.Padding + IconSize / 2, value.Y);
				actionText.Position = new Vector2(actionSprite.Position.X + IconSize / 2 + UIConstants.Padding, value.Y);

				base.Position = value;
			}
		}

		public override void Draw(SuperBatch sb)
		{
			actionSprite.Draw(sb);
			actionText.Draw(sb);

			Primitives.DrawBounds(sb, Bounds, Color.White);
		}
	}
}
