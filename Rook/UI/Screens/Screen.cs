﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Core;
using Rook.Interfaces;

namespace Rook.UI.Screens
{
	public abstract class Screen : IDynamic, IRenderable
	{
		protected Screen()
		{
			Containers = new List<Container2D>();
		}

		protected List<Container2D> Containers { get; }

		public virtual void Update(float dt)
		{
			foreach (Container2D container in Containers)
			{
				if (container.Visible)
				{
					container.Update(dt);
				}
			}
		}

		public virtual void Draw(SuperBatch sb)
		{
			foreach (Container2D container in Containers)
			{
				if (container.Visible)
				{
					container.Draw(sb);
				}
			}
		}
	}
}
