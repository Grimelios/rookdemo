﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;

namespace Rook.UI
{
	public class DebugDisplay : Container2D
	{
		private SpriteFont font;
		private List<SpriteText> textList;

		public DebugDisplay()
		{
			font = ContentLoader.LoadFont("Debug");
			textList = new List<SpriteText>();

			Messaging.Subscribe(MessageTypes.Debug, (data, dt) => Add((string)data));
		}

		private void Add(string value)
		{
			textList.Add(new SpriteText(font, value)
			{
				Position = Position + new Vector2(0, 16 * textList.Count)
			});
		}

		public void Clear()
		{
			textList.Clear();
		}

		public override void Draw(SuperBatch sb)
		{
			textList.ForEach(t => t.Draw(sb));
		}
	}
}
