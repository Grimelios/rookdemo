﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Core;

namespace Rook
{
	public static class Primitives
	{
		private static Texture2D pixel;

		static Primitives()
		{
			pixel = ContentLoader.LoadTexture("Pixel");
		}

		public static void DrawLine(SuperBatch sb, Vector2 start, Vector2 end, Color color)
		{
			start = start.ToIntegers();
			end = end.ToIntegers();

			float length = Vector2.Distance(start, end);
			float rotation = Functions.ComputeAngle(start, end);

			Rectangle sourceRect = new Rectangle(0, 0, (int)length, 1);

			sb.Draw(pixel, start, sourceRect, color, rotation);
		}

		public static void DrawBounds(SuperBatch sb, Bounds bounds, Color color)
		{
			Vector2 topLeft = new Vector2(bounds.Left, bounds.Top);
			Vector2 topRight = new Vector2(bounds.Right, bounds.Top);
			Vector2 bottomLeft = new Vector2(bounds.Left, bounds.Bottom);
			Vector2 bottomRight = new Vector2(bounds.Right, bounds.Bottom);

			DrawLine(sb, topLeft, topRight, color);
			DrawLine(sb, topLeft, bottomLeft, color);
			DrawLine(sb, topRight, bottomRight, color);
			DrawLine(sb, bottomLeft, bottomRight, color);
		}

		public static void FillBounds(SuperBatch sb, Bounds bounds, Color color)
		{
			sb.Draw(pixel, bounds.ToRectangle(), color);
		}

		public static void FillBounds(SuperBatch sb, Vector2 position, Bounds bounds, Color color)
		{
			sb.Draw(pixel, position, bounds.ToRectangle(), color);
		}
	}
}
