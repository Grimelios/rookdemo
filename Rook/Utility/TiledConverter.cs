﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Entities;
using Rook.Json;
using Rook.Physics;
using Rook.Physics.Shapes;

namespace Rook.Utility
{
	public class TiledConverter
	{
		private const uint HMask = (uint)1 << 31;
		private const uint VMask = (uint)1 << 30;

		public TiledConverter()
		{
			Directory.CreateDirectory(Paths.Json + "Tilemaps");
		}

		public void Convert(string filename)
		{
			XDocument document = XDocument.Load(Environment.CurrentDirectory + "/../../../../../Tiled/" + filename);
			XElement root = document.Root;

			int width = int.Parse(root.Attribute("width").Value);
			int height = int.Parse(root.Attribute("height").Value);
			int tileSize = int.Parse(root.Attribute("tilewidth").Value);
			int[,] tiles = new int[width, height];

			SpriteEffects[,] flipData = new SpriteEffects[width, height];

			string tilesheet = Functions.RemoveExtension(root.Element("tileset").Attribute("source").Value);
			string rawFilename = Functions.RemoveExtension(filename);
			string[] tileLines = root.Element("layer").Element("data").Value.Split('\n');

			for (int i = 0; i < height; i++)
			{
				// The first line will always be blank.
				string[] tokens = tileLines[i + 1].Split(',');

				for (int j = 0; j < width; j++)
				{
					uint value = uint.Parse(tokens[j]);

					SpriteEffects effects = SpriteEffects.None;

					if ((value & HMask) > 0)
					{
						effects |= SpriteEffects.FlipHorizontally;
						value -= HMask;
					}

					if ((value & VMask) > 0)
					{
						effects |= SpriteEffects.FlipVertically;
						value -= VMask;
					}

					// Tiled stores 0 as an empty tile rather than -1.
					tiles[j, i] = (int)value - 1;
					flipData[j, i] = effects;
				}
			}

			TilemapData data = new TilemapData(width, height, tileSize, tiles, flipData, tilesheet, ParseEdges(root));
			JsonUtilities.Serialize(data, $"Tilemaps/{rawFilename}.json");
		}

		private Edge[][] ParseEdges(XElement root)
		{
			XElement layerElement = root.Element("objectgroup");

			if (layerElement == null)
			{
				return null;
			}

			XElement[] objectElements = layerElement.Elements("object").ToArray();
			Edge[][] edges = new Edge[objectElements.Length][];

			for (int i = 0; i < objectElements.Length; i++)
			{
				XElement objectElement = objectElements[i];

				string[] tokens = objectElement.Element("polyline").Attribute("points").Value.Split(' ');

				float baseX = float.Parse(objectElement.Attribute("x").Value);
				float baseY = float.Parse(objectElement.Attribute("y").Value);

				Vector2 basePosition = new Vector2(baseX, baseY);
				Vector2[] points = new Vector2[tokens.Length];

				for (int j = 0; j < points.Length; j++)
				{
					string[] subTokens = tokens[j].Split(',');

					float x = float.Parse(subTokens[0]);
					float y = float.Parse(subTokens[1]);

					points[j] = basePosition + new Vector2(x, y);
				}

				Edge[] edgeArray = new Edge[points.Length - 1];

				for (int j = 0; j < edgeArray.Length; j++)
				{
					edgeArray[j] = new Edge(points[j], points[j + 1]);
				}

				edges[i] = edgeArray;
			}

			return edges;
		}
	}
}
