﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Data
{
	public enum FieldTypes
	{
		Integer,
		Float,
		Color
	}

	public class DataField
	{
		public DataField(FieldTypes type, string key)
		{
			Type = type;
			Key = key;
		}

		public FieldTypes Type { get; }

		public string Key { get; }
	}
}
