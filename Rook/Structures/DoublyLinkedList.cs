﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Structures
{
	public class DoublyLinkedList<T>
	{
		public DoublyLinkedListNode<T> Head { get; private set; }
		public DoublyLinkedListNode<T> Tail { get; private set; }

		public int Count { get; private set; }

		public DoublyLinkedList()
		{
		}

		public DoublyLinkedList(IEnumerable<T> items)
		{
			foreach (T item in items)
			{
				Add(item);
			}
		}

		public void Clear()
		{
			Head = null;
			Tail = null;
			Count = 0;
		}

		public void Add(T data)
		{
			DoublyLinkedListNode<T> node = new DoublyLinkedListNode<T>(data);

			if (Count == 0)
			{
				Head = node;
				Tail = node;
			}
			else
			{
				node.Previous = Tail;
				Tail.Next = node;
				Tail = node;
			}

			Count++;
		}
	}
}
