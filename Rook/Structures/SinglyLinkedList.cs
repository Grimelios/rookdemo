﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Structures
{
	public class SinglyLinkedList<T>
	{
		public SinglyLinkedList()
		{
		}

		public SinglyLinkedList(IEnumerable<T> items)
		{
			foreach (T item in items)
			{
				Add(item);
			}
		}

		public SinglyLinkedListNode<T> Head { get; private set; }
		public SinglyLinkedListNode<T> Tail { get; private set; }

		public int Count { get; private set; }

		public void Add(T data)
		{
			SinglyLinkedListNode<T> node = new SinglyLinkedListNode<T>(data);

			if (Count == 0)
			{
				Head = node;
				Tail = node;
			}
			else
			{
				Tail.Next = node;
				Tail = node;
			}

			Count++;
		}
	}
}
