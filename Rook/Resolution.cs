﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook
{
	public static class Resolution
	{
		public const int Width = 800;
		public const int Height = 600;

		public static int WindowWidth { get; private set; } = Width;
		public static int WindowHeight { get; private set; } = Height;

		public static Vector2 WindowDimensions => new Vector2(WindowWidth, WindowHeight);
		public static Vector2 WindowCenter => WindowDimensions / 2;
		public static Vector2 Dimensions => new Vector2(Width, Height);
		public static Vector2 Center => Dimensions / 2;

		public static void Resize(int width, int height)
		{
			WindowWidth = width;
			WindowHeight = height;

			Messaging.Send(MessageTypes.Resize, new Point(width, height));
		}
	}
}
