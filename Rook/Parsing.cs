﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook
{
	public static class Parsing
	{
		public static Color ParseColor(string value)
		{
			string[] tokens = value.Split(',');

			// Single values are assumed greyscale.
			if (tokens.Length == 1)
			{
				int lightness = int.Parse(tokens[0]);

				return new Color(lightness, lightness, lightness);
			}

			// If alpha is omitted, it's assumed fully opaque.
			int r = int.Parse(tokens[0]);
			int g = int.Parse(tokens[1]);
			int b = int.Parse(tokens[2]);
			int a = tokens.Length == 4 ? int.Parse(tokens[3]) : 255;

			return new Color(r, g, b, a);
		}

		public static Rectangle ParseRectangle(string value)
		{
			string[] tokens = value.Split(',');

			int x = int.Parse(tokens[0]);
			int y = int.Parse(tokens[1]);
			int width = int.Parse(tokens[2]);
			int height = int.Parse(tokens[3]);

			return new Rectangle(x, y, width, height);
		}
	}
}
