﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook
{
	using Receiver = Action<object, float>;
	using ReceiverData = Tuple<MessageTypes, Action<object, float>>;

	public enum MessageTypes
	{
		Keyboard,
		Mouse,
		Xbox360,
		Input,
		Pause,
		Save,
		Debug,
		Resize,
		Exit
	}

	public static class Messaging
	{
		private static List<Receiver>[] receivers;
		private static List<ReceiverData> addList;
		private static List<ReceiverData> removeList;

		static Messaging()
		{
			receivers = new List<Receiver>[Functions.EnumCount<MessageTypes>()];
			addList = new List<ReceiverData>();
			removeList = new List<ReceiverData>();
		}

		public static void Subscribe(MessageTypes messageType, Receiver receiver)
		{
			addList.Add(new ReceiverData(messageType, receiver));
		}

		public static void Unsubscribe(MessageTypes messageType, Receiver receiver)
		{
			removeList.Add(new ReceiverData(messageType, receiver));
		}

		public static void Send(MessageTypes messageType, object data, float dt = 0)
		{
			VerifyList(messageType).ForEach(r => r(data, dt));
		}

		public static void ProcessChanges()
		{
			removeList.ForEach(r => receivers[(int)r.Item1].Remove(r.Item2));
			removeList.Clear();

			addList.ForEach(r => VerifyList(r.Item1).Add(r.Item2));
			addList.Clear();
		}

		private static List<Receiver> VerifyList(MessageTypes messageType)
		{
			int index = (int)messageType;

			return receivers[index] ?? (receivers[index] = new List<Receiver>());
		}
	}
}
