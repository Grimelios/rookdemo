﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Core
{
	public class Timer : IDynamic
	{
		private Action<float> tick;
		private Action<float> trigger;
		private Func<float, bool> repeatingTrigger;

		private float elapsed;

		public Timer(float duration, Action<float> trigger, Action<float> tick = null, float initialElapsed = 0)
		{
			this.trigger = trigger;
			this.tick = tick;

			elapsed = initialElapsed;
			Duration = duration;
		}

		public Timer(float duration, Func<float, bool> repeatingTrigger, Action<float> tick = null, float initialElapsed = 0)
		{
			this.repeatingTrigger = repeatingTrigger;
			this.tick = tick;

			elapsed = initialElapsed;
			Duration = duration;
		}

		public float Duration { get; set; }

		public bool Paused { get; set; }
		public bool Complete { get; private set; }

		public void Update(float dt)
		{
			if (Paused)
			{
				return;
			}

			elapsed += dt * 1000;

			if (elapsed >= Duration)
			{
				elapsed -= Duration;

				if (trigger != null)
				{
					trigger(elapsed);
					Complete = true;
				}
				else if (!repeatingTrigger(elapsed))
				{
					Complete = true;
				}
			}

			if (!Complete)
			{
				tick?.Invoke(elapsed / Duration);
			}
		}
	}
}
