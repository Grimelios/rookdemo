﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Rook.Core
{
	public class Sprite : Component2D
	{
		private Texture2D texture;
		private Vector2 origin;
		private Rectangle? sourceRect;

		private bool flipHorizontally;
		private bool flipVertically;

		private SpriteEffects effects;

		public Sprite(string texture, Rectangle? sourceRect = null, Alignments alignment = Alignments.Center) :
			this(ContentLoader.LoadTexture(texture), sourceRect, alignment)
		{
		}

		public Sprite(Texture2D texture, Rectangle? sourceRect= null, Alignments alignment = Alignments.Center)
		{
			this.texture = texture;
			this.sourceRect = sourceRect;

			origin = sourceRect.HasValue
				? Functions.ComputeOrigin(sourceRect.Value.Width, sourceRect.Value.Height, alignment)
				: Functions.ComputeOrigin(texture.Width, texture.Height, alignment);
			effects = SpriteEffects.None;
		}

		public bool FlipHorizontal
		{
			get => flipHorizontally;
			set
			{
				flipHorizontally = value;
				RecomputeEffects();
			}
		}

		public bool FlipVertical
		{
			get => flipVertically;
			set
			{
				flipVertically = value;
				RecomputeEffects();
			}
		}

		private void RecomputeEffects()
		{
			effects = SpriteEffects.None;

			if (flipHorizontally)
			{
				effects |= SpriteEffects.FlipHorizontally;
			}

			if (flipVertically)
			{
				effects |= SpriteEffects.FlipVertically;
			}
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Draw(texture, Position.ToIntegers(), origin, sourceRect, Color, Rotation, Scale, effects);
		}
	}
}
