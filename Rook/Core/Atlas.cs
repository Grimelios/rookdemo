﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Json;

namespace Rook.Core
{
	public class Atlas
	{
		private Dictionary<string, Rectangle> map;

		public Atlas(string filename)
		{
			map = new Dictionary<string, Rectangle>();

			foreach (KeyValuePair<string, string> pair in JsonUtilities.Deserialize<Dictionary<string, string>>("Atlases/" + filename))
			{
				map.Add(pair.Key, Parsing.ParseRectangle(pair.Value));
			}
		}

		public Rectangle this[string key] => map[key];
	}
}
