﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Rook.Core
{
	public class SpriteText : Component2D
	{
		private SpriteFont font;
		private Vector2 origin;

		private string value;

		private Alignments alignment;

		public SpriteText(string font, string value, Alignments alignment = Alignments.Left | Alignments.Top) :
			this(ContentLoader.LoadFont(font), value, alignment)
		{
		}

		public SpriteText(SpriteFont font, string value, Alignments alignment = Alignments.Left | Alignments.Top)
		{
			this.font = font;
			this.alignment = alignment;

			Value = value;
		}

		public string Value
		{
			get => value;
			set
			{
				this.value = value;

				if (value != null)
				{
					Vector2 dimensions = font.MeasureString(value);

					origin = Functions.ComputeOrigin((int)dimensions.X, (int)dimensions.Y, alignment);
				}
			}
		}

		public override void Draw(SuperBatch sb)
		{
			if (value != null)
			{
				sb.DrawString(font, value, Position.ToIntegers(), origin, Color, Rotation, Scale);
			}
		}
	}
}
