﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Input.Data;
using Rook.Interfaces;

namespace Rook.Core
{
	public abstract class Container2D : IBoundable, IDynamic, IRenderable
	{
		private Vector2 position;

		protected bool Centered { get; set; }

		public virtual Vector2 Position
		{
			get => position;
			set
			{
				position = value;

				Point point = value.ToPoint();

				if (Centered)
				{
					Bounds.Center = point;
				}
				else
				{
					Bounds.Location = point;
				}
			}
		}

		public Bounds Bounds { get; protected set; } = new Bounds();

		public bool Visible { get; set; } = true;

		public virtual void Update(float dt)
		{
		}

		public virtual void Draw(SuperBatch sb)
		{
		}
	}
}
