﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;

namespace Rook.Core
{
	public abstract class Component2D : IPositionable, IRotatable, IScalable, IColorable, IDynamic, IRenderable
	{
		protected Component2D()
		{
			Scale = 1;
			Color = Color.White;
		}

		public Vector2 Position { get; set; }

		public float Rotation { get; set; }
		public float Scale { get; set; }

		public Color Color { get; set; }

		public virtual void Update(float dt)
		{
		}

		public virtual void Draw(SuperBatch sb)
		{
		}
	}
}
