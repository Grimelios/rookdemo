﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Interfaces;
using Rook.Shapes;
using Rook.UI;

namespace Rook.Sensors
{
	[Flags]
	public enum SensorTypes
	{
		Attack = 1,
		Player = 2,
		Enemy = 4,
		Character = 8,
		Living = 14,
		Mechanism = 16,
		Interactive = 25,
		All = 31
	}

	[DebuggerDisplay("{SensorType}, {UserData}")]
	public class Sensor : IPositionable, IRotatable, IDisposable
	{
		public delegate void SensorContactHandler(Sensor other);
		public delegate void SensorSeparationHandler(Sensor other);
		public delegate void SensorDisposalHandler();
		
		public Sensor(SensorTypes sensorType)
		{
			SensorType = sensorType;
			ContactList = new List<Sensor>();
			IgnoreList = new List<Sensor>();
		}

		public Vector2 Position
		{
			get => Shape.Position;
			set => Shape.Position = value;
		}

		public float Rotation
		{
			get => Shape.Rotation;
			set => Shape.Rotation = value;
		}

		public SensorTypes SensorType { get; }
		public SensorTypes CollidesWith { get; protected set; } = SensorTypes.All;

		public bool Enabled { get; set; } = true;

		public AbstractShape Shape { get; set; }
		public SensorManager Manager { get; set; }
		public List<Sensor> ContactList { get; }
		public List<Sensor> IgnoreList { get; }

		public object UserData { get; set; }

		public event SensorContactHandler OnContact;
		public event SensorSeparationHandler OnSeparation;
		public event SensorDisposalHandler OnDisposal;

		public void IgnoreCollisionWith(Sensor sensor)
		{
			IgnoreList.Add(sensor);
		}

		public virtual void TriggerContact(Sensor sensor)
		{
			ContactList.Add(sensor);
			OnContact?.Invoke(sensor);
		}

		public virtual void TriggerSeparation(Sensor sensor)
		{
			ContactList.Remove(sensor);
			OnSeparation?.Invoke(sensor);
		}

		public void Dispose()
		{
			Manager.Remove(this);
			OnDisposal?.Invoke();
		}
	}
}
