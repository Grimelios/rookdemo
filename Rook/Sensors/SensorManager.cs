﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Sensors
{
	public class SensorManager : IDynamic
	{
		private List<Sensor> addList;
		private List<Sensor> removeList;

		public SensorManager()
		{
			addList = new List<Sensor>();
			removeList = new List<Sensor>();
			SensorList = new List<Sensor>();
		}

		public List<Sensor> SensorList { get; }

		public void Add(Sensor sensor)
		{
			addList.Add(sensor);
		}

		public void Remove(Sensor sensor)
		{
			removeList.Add(sensor);
		}

		public void Update(float dt)
		{
			for (int i = 0; i < SensorList.Count; i++)
			{
				Sensor sensor1 = SensorList[i];

				if (!sensor1.Enabled)
				{
					continue;
				}

				for (int j = i + 1; j < SensorList.Count; j++)
				{
					Sensor sensor2 = SensorList[j];

					if (!sensor2.Enabled)
					{
						continue;
					}

					// Sensor contacts are reflective (just like physics contacts).
					if (sensor1.ContactList.Contains(sensor2))
					{
						if (!sensor1.Shape.Intersects(sensor2.Shape))
						{
							sensor1.TriggerSeparation(sensor2);
							sensor2.TriggerSeparation(sensor1);
						}

						continue;
					}

					if (sensor1.IgnoreList.Contains(sensor2) || sensor2.IgnoreList.Contains(sensor1) || !SensorsCollide(sensor1, sensor2))
					{
						continue;
					}

					if (sensor1.Shape.Intersects(sensor2.Shape))
					{
						sensor1.TriggerContact(sensor2);
						sensor2.TriggerContact(sensor1);
					}
				}
			}

			removeList.ForEach(s => SensorList.Remove(s));
			removeList.Clear();

			addList.ForEach(s => SensorList.Add(s));
			addList.Clear();
		}

		private bool SensorsCollide(Sensor sensor1, Sensor sensor2)
		{
			return (sensor1.CollidesWith & sensor2.SensorType) > 0 && (sensor2.CollidesWith & sensor1.SensorType) > 0;
		}
	}
}
