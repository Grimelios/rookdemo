﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Collision.Shapes;
using Microsoft.Xna.Framework;
using Rook.Interfaces;
using Rook.Shapes;

namespace Rook.Sensors
{
	public class SensorVisualizer : IRenderable
	{
		private SensorManager sensorManager;
		private Color color;

		public SensorVisualizer(SensorManager sensorManager)
		{
			this.sensorManager = sensorManager;

			color = Color.HotPink;
		}

		public bool Enabled { get; set; } = true;

		public void Draw(SuperBatch sb)
		{
			foreach (Sensor sensor in sensorManager.SensorList)
			{
				AbstractShape shape = sensor.Shape;

				switch (shape.Type)
				{
					case ShapeTypes.Sector: DrawSector(sb, (Sector)shape);
						break;

					case ShapeTypes.Box: DrawBox(sb, (Box)shape);
						break;

					case ShapeTypes.Circle: DrawCircle(sb, (Circle)shape);
						break;
				}
			}
		}

		private void DrawSector(SuperBatch sb, Sector sector)
		{
			const int Segments = 10;

			float start = sector.Rotation - sector.Spread / 2;
			float increment = sector.Spread / Segments;

			Vector2 position = sector.Position;
			Vector2[] points = new Vector2[Segments + 1];

			for (int i = 0; i <= Segments; i++)
			{
				points[i] = position + Functions.ComputeDirection(start + increment * i) * sector.Radius;
			}

			for (int i = 0; i < Segments; i++)
			{
				Primitives.DrawLine(sb, points[i], points[i + 1], color);
			}

			Primitives.DrawLine(sb, position, points[0], color);
			Primitives.DrawLine(sb, position, points.Last(), color);
		}

		private void DrawBox(SuperBatch sb, Box box)
		{
			Matrix rotationMatrix = Matrix.CreateRotationZ(box.Rotation);

			float halfWidth = box.Width / 2;
			float halfHeight = box.Height / 2;

			Vector2[] corners =
			{
				new Vector2(-halfWidth, -halfHeight),
				new Vector2(halfWidth, -halfHeight),
				new Vector2(halfWidth, halfHeight),
				new Vector2(-halfWidth, halfHeight)
			};

			for (int i = 0; i < corners.Length; i++)
			{
				corners[i] = box.Position + Vector2.Transform(corners[i], rotationMatrix);
			}

			for (int i = 0; i < corners.Length; i++)
			{
				Vector2 point1 = corners[i];
				Vector2 point2 = corners[i == 3 ? 0 : i + 1];

				Primitives.DrawLine(sb, point1, point2, color);
			}
		}

		private void DrawCircle(SuperBatch sb, Circle circle)
		{
		}
	}
}
