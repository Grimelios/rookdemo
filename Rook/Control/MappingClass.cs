﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Rook.Input;
using Rook.Input.Data;

namespace Rook.Control
{
	public abstract class MappingClass
	{
		protected HashSet<Keys> GetMappedKeys(List<InputBind> binds)
		{
			HashSet<Keys> mappedKeys = new HashSet<Keys>();

			foreach (InputBind bind in binds)
			{
				if (bind.InputType == InputTypes.Keyboard)
				{
					mappedKeys.Add((Keys)bind.Data);
				}
			}

			return mappedKeys;
		}

		public abstract HashSet<Keys> GetMappedKeys();
	}
}
