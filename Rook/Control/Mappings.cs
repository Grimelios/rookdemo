﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Entities;
using Rook.Json;
using Rook.UI.Menus;

namespace Rook.Control
{
	public static class Mappings
	{
		private const string Folder = "Controls/";

		static Mappings()
		{
			Player = JsonUtilities.Deserialize<PlayerControls>(Folder + "DefaultPlayerControls.json");
			Menu = JsonUtilities.Deserialize<MenuControls>(Folder + "DefaultMenuControls.json");
		}

		public static PlayerControls Player { get; }
		public static MenuControls Menu { get; }
	}
}
