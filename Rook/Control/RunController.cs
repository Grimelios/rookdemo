﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Entities;
using Rook.Entities.Core;
using Rook.Interfaces;
using Rook.Physics;
using Rook.Physics.Shapes;

namespace Rook.Control
{
	public class RunController : IDynamic
	{
		private Edge edge;
		private Edge next;
		private Edge previous;
		private Vector2 parentPosition;
		private Vector2 start;
		private Vector2 end;
		private Player player;

		private float currentSpeed;
		private float edgeProgress;
		private float wallThreshold;

		private int flip;

		private bool previousDropoff;
		private bool previousWall;
		private bool nextDropoff;
		private bool nextWall;

		public RunController(Player player)
		{
			this.player = player;

			// A collidable wall is defined as one that's steeper than a segment of the player's diamond hitbox.
			wallThreshold = (float)player.Bounds.Height / player.Bounds.Width;
		}

		public int Acceleration { get; set; }
		public int Deceleration { get; set; }
		public int MaxSpeed { get; set; }

		public bool Accelerating { get; set; }

		public int Direction { get; set; }

		// This computes the current speed in the X direction.
		public float CurrentSpeed => currentSpeed * Math.Abs((float)Math.Cos(Functions.ComputeAngle(edge.Start, edge.End))) * flip;

		public void SignalLanding(Entity entity, Edge edge, float bodyX, float bodySpeed)
		{
			// Edge endpoints are local to their containing body, so they need to be corrected to world space.
			parentPosition = entity.Position;

			SetEdge(edge);

			// The flip value can be computed once for the entire connected edge group.
			flip = start.X > end.X ? -1 : 1;
			edgeProgress = ComputeEdgeProgress(PhysicsConvert.ToPixels(bodyX));
			currentSpeed = PhysicsConvert.ToPixels(bodySpeed) * flip;
		}

		public void SignalSeparation()
		{
			edge = null;
		}

		private void SetEdge(Edge edge)
		{
			this.edge = edge;

			next = edge.Next;
			previous = edge.Previous;
			start = parentPosition + edge.Start;
			end = parentPosition + edge.End;

			RecomputeNeighborData(next, -1, out nextWall, out nextDropoff);
			RecomputeNeighborData(previous, 1, out previousWall, out previousDropoff);
		}

		private void RecomputeNeighborData(Edge edge, int check, out bool wall, out bool dropoff)
		{
			if (edge == null)
			{
				wall = false;
				dropoff = true;

				return;
			}

			float? slope = edge.Slope;

			if (slope == null || Math.Abs(slope.Value) > wallThreshold)
			{
				wall = Math.Sign(edge.End.Y - edge.Start.Y) == check;
				dropoff = !wall;

				return;
			}

			wall = false;
			dropoff = false;
		}

		private float ComputeEdgeProgress(float x)
		{
			return (x - start.X) / edge.Width * edge.Length * flip;
		}

		public void Update(float dt)
		{
			if (player.OnGround)
			{
				AdvanceGround(dt);
			}
			else
			{
				AdvanceAir();
			}
		}
	
		private void AdvanceGround(float dt)
		{
			if (Accelerating)
			{
				currentSpeed += player.Acceleration * dt * (Direction == 1 ? 1 : -1) * flip;
				currentSpeed = MathHelper.Clamp(currentSpeed, -player.MaxSpeed, player.MaxSpeed);
			}
			else if (currentSpeed != 0)
			{
				int previousSign = Math.Sign(currentSpeed);

				currentSpeed += player.Deceleration * dt * -previousSign;

				if (Math.Sign(currentSpeed) != previousSign)
				{
					currentSpeed = 0;
				}
			}

			edgeProgress += currentSpeed * dt;

			if (edgeProgress < 0)
			{
				if (previousDropoff)
				{
					player.SignalDropoff();
				}
				else
				{
					SetEdge(previous);
					edgeProgress += edge.Length;
				}
			}
			else if (edgeProgress > edge.Length)
			{
				if (nextDropoff)
				{
					player.SignalDropoff();
				}
				else
				{
					float previousLength = edge.Length;

					SetEdge(next);
					edgeProgress -= previousLength;
				}
			}
			else
			{
				Edge wall = null;

				if (currentSpeed < 0 && previousWall)
				{
					wall = previous;
				}
				else if (currentSpeed > 0 && nextWall)
				{
					wall = next;
				}

				Vector2 tentativePoint = Vector2.Lerp(start, end, edgeProgress / edge.Length);
				
				if (wall != null && CheckCollision(tentativePoint, wall, Math.Sign(currentSpeed) * flip, out float correctedX))
				{
					currentSpeed = 0;
					edgeProgress = ComputeEdgeProgress(correctedX);
					player.GroundPoint = new Vector2(correctedX, parentPosition.Y + edge.ComputeY(correctedX));
				}
				else
				{
					player.GroundPoint = tentativePoint;
				}
			}
		}

		private bool CheckCollision(Vector2 tentativePoint, Edge wall, int sign, out float correctedX)
		{
			Vector2 sidePoint = tentativePoint + new Vector2(player.Bounds.Width * sign, player.Bounds.Height) / 2;

			if (!wall.Slope.HasValue)
			{
				float wallX = parentPosition.X + wall.Start.X;
				float delta = sidePoint.X - wallX;

				if (delta == 0 || Math.Sign(delta) == sign)
				{
					correctedX = parentPosition.X + wall.Start.X + player.Bounds.Width / 2 * -sign - sign;

					return true;
				}
			}
			else
			{
				float wallX = parentPosition.X + wall.ComputeX(sidePoint.Y);
				float delta = sidePoint.X - wallX;

				if (delta == 0 || Math.Sign(delta) == sign)
				{
					correctedX = 0;

					return true;
				}
			}

			correctedX = 0;

			return false;
		}

		private void AdvanceAir()
		{
			Body body = player.Body;

			if (body.Accelerating)
			{
				float acceleration = PhysicsConvert.ToMeters(Acceleration) * Direction;

				body.ApplyRawForce(new Vector2(acceleration, 0));
			}
			else if (body.Decelerating)
			{
				float deceleration = PhysicsConvert.ToMeters(Deceleration) * -Math.Sign(body.LinearVelocity.X);

				body.ApplyRawForce(new Vector2(deceleration, 0));
			}
		}
	}
}
